/**
 * @file integer.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for integer module. Integer type definitions for FatFs module
 * @version 0.1
 * @date 2022-01-10
 *
 * @copyright Copyright (C) 2010, ChaN
 *
 */

#ifndef _INTEGER
#define _INTEGER

#ifdef _WIN32 /* FatFs development platform */

#include <tchar.hh>
#include <windows.hh>

#else /* Embedded platform */

/* These types must be 16-bit, 32-bit or larger integer */
typedef int INT;
typedef unsigned int UINT;

/* These types must be 8-bit integer */
typedef char CHAR;
typedef unsigned char UCHAR;
typedef unsigned char BYTE;

/* These types must be 16-bit integer */
typedef short SHORT;
typedef unsigned short USHORT;
typedef unsigned short WORD;
typedef unsigned short WCHAR;

/* These types must be 32-bit integer */
typedef long LONG;
typedef unsigned long ULONG;
typedef unsigned long DWORD;

#endif

#endif
