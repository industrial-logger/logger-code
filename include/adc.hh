/**
 * @file adc.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for ADC module
 * @version 0.1
 * @date 2022-01-28
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <avr/io.h>
#include <util/delay.h>

#ifndef ADC_HH_
#define ADC_HH_

class ADC_
{
  private:
    /**
     * @brief Variable to hold the measurement
     *
     */
    double measurement = 0;

  public:
    /**
     * @brief Construct a new ADC_ object
     *
     */
    ADC_();

    /**
     * @brief Measure value of ADC
     *
     * @return uint8_t read value
     */
    double read();

    /**
     * @brief Destroy the ADC_ object
     *
     */
    ~ADC_(){};
};

#endif /* ADC_HH_ */
