/**
 * @file rfid.hh
 * @authors Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for RFID scanner module
 * @version 0.1
 * @date 2021-10-21
 *
 * @Copyright Copyright (C) 2021
 *
 */

#include <avr/io.h>
#include <util/delay.h>

#include "rfid_reg.hh"
#include "spi.hh"

#ifndef RFID_HH_
#define RFID_HH_

#if(!defined _RFID_DDR || !defined _RFID_PORT)
#define _RFID_DDR  DDRB
#define _RFID_PORT PORTB
#define _RFID_PIN  PINB
#endif

#if(!defined _CS_RFID || !defined _RFID_RST)
#define _RFID_SS  _SPI_CS_2
#define _RFID_RST 2
#endif

/**
 * @brief Structure used for passing the UID of a PICC.
 */
struct UID
{
    uint8_t size;         /** @brief Number of bytes in the UID; 4, 7, 10 */
    uint8_t uid_byte[10]; /** @brief Array to store uid bytes. */
    uint8_t sak;          /** @brief The SAK (Select acknowledge) byte returned from PICC successful selection. */
};

class RFID
{
  private:
    /**
     * @brief Structure to save informations about card uid
     */
    UID uid;

    /**
     * @brief Enable RFID chip for SPI communication
     */
    void enable();

    /**
     * @brief Disable RFID chip for SPI communication
     */
    void disable();

    /**
     * @brief Turns the antenna on by enabling pins TX1 and TX2.
     */
    void antenna_on();

    /**
     * @brief Sets the bits given in mask in register reg.
     * @param reg the register to update
     * @param mask the bits to set
     */
    void set_bit_mask(uint8_t reg, uint8_t mask);

    /**
     * @brief Clears the bits given in mask from register reg.
     * @param reg the register to update
     * @param mask the bits to clear
     */
    void clear_bit_mask(uint8_t reg, uint8_t mask);

    /**
     * @brief Reads a byte from the specified register in the MFRC522 chip.
     * @param address the register to read from
     * @return uint8_t byte read from register
     */
    uint8_t MFRC522_read(uint8_t address);

    /**
     * @brief Reads a number of bytes from the specified register in the MFRC522 chip.
     * @details The interface is described in the datasheet section 8.1.2.
     * @param address the register to read from
     * @param count the number of bytes to read
     * @param values byte array to store the values in
     * @param rx_align only bit positions rxAlign..7 in values[0] are updated
     */
    void MFRC522_read(uint8_t address, uint8_t count, uint8_t* values, uint8_t rx_align);

    /**
     * @brief Writes a byte to the specified register in the MFRC522 chip.
     * @param address the register to write to
     * @param value the value to write
     */
    void MFRC522_write(uint8_t address, uint8_t value);

    /**
     * @brief Writes a n bytes to the specified register in the MFRC522 chip.
     * @param address the register to write to
     * @param count number of bytes to write
     * @param values the values to write
     */
    void MFRC522_write(uint8_t address, const uint8_t count, uint8_t* const values);

    /**
     * @brief Communicate with the card.
     * @details Transfers data to the MFRC522 FIFO, executes a command, waits for completion and transfers data back
     * from the FIFO. CRC validation can only be done if backData and backLen are specified.
     * @param command the command to execute
     * @param waitIRq the bits in the ComIrqReg register that signals succesful completion of the command
     * @param send_data pointer to the data to transfer to the FIFO
     * @param send_length number of bytes to transfer to the FIFO
     * @param back_data nullptr or pointer to the buffer if data should be read back after executing the command
     * [default value = nullptr]
     * @param back_length IN: max number of bytes to write to *back_data; OUT: the number of bytes returned [default
     * value = nullptr]
     * @param valid_bits IN/OUT: the number of valid bits in the last byte; 0 for 8 valid bits [default value = nullptr]
     * @param rx_align defines the bit position in back_data[0] for the first bit received [default value = 0]
     * @param check_CRC TRUE => the last two bytes of the response is assumed to be a CRC_A that mast be validated
     * [default value = false]
     */
    uint8_t MFRC522_communicate(uint8_t command, uint8_t waitIRq, uint8_t* send_data, uint8_t send_length,
                                uint8_t* back_data = nullptr, uint8_t* back_length = nullptr,
                                uint8_t* valid_bits = nullptr, uint8_t rx_align = 0, bool check_CRC = false);

    /**
     * @brief Executes the Transceive command.
     * @details CRC validation can only be done if back_data and back_length are specified.
     * @param send_data pointer to the data to transfer to the FIFO
     * @param send_length number of bytes to transfer to the FIFO
     * @param back_data nullptr or pointer to the buffer if data should be read back after executing the command
     * [default value = nullptr]
     * @param back_length IN: max number of bytes to write to *back_data; OUT: the number of bytes returned [default
     * value = nullptr]
     * @param valid_bits IN/OUT: the number of valid bits in the last byte; 0 for 8 valid bits [default value = nullptr]
     * @param rx_align defines the bit position in back_data[0] for the first bit received [default value = 0]
     * @param check_CRC TRUE => the last two bytes of the response is assumed to be a CRC_A that mast be validated
     * [default value = false]
     */
    uint8_t MFRC522_transceive_data(uint8_t* send_data, uint8_t send_length, uint8_t* back_data = nullptr,
                                    uint8_t* back_length = nullptr, uint8_t* valid_bits = nullptr, uint8_t rx_align = 0,
                                    bool check_CRC = false);

    /**
     * @brief Transmits REQA or WUPA commands.
     * @param command the command to send
     * @param buffer_answer_request the buffer to store ATQA (Answer to request) in
     * @param buffer_size buffer size; at least two bytes; also number of bytes returned if success
     * @return uint8_t - Status of request
     */
    uint8_t MFRC522_request_wakeup(uint8_t command, uint8_t* buffer_answer_request, uint8_t* buffer_size);

    /**
     * @brief Transmit a REQuest command, type A.
     * @details Invites PICCs in state IDLE to go to READY and prepare for anticollision or selection. 7 bit frame.
     * @param buffer_answer_request the buffer to store ATQA (Answer to request) in
     * @param buffer_size buffer size; at least two bytes; also number of bytes returned if success
     * @return uint8_t - Status of request
     */
    uint8_t request(uint8_t* buffer_answer_request, uint8_t* buffer_size);

    /**
     * @brief Transmits a WakeUP command, type A.
     * @details Invites PICCs in state IDLE and HALT to go to READY(*) and prepare for anticollision or selection. 7 bit
     * frame.
     * @param buffer_answer_request the buffer to store ATQA (Answer to request) in
     * @param buffer_size buffer size; at least two bytes; also number of bytes returned if success
     * @return uint8_t - Status of request
     */
    uint8_t wake_up(uint8_t* buffer_answer_request, uint8_t* buffer_size);

    /**
     * @brief Use the CRC coprocessor in the MFRC522 to calculate a CRC_A.
     * @param data pointer to the data to transfer to the FIFO for CRC calculation
     * @param length the number of bytes to transfer
     * @param result pointer to result buffer; result is written to result[0..1], low byte first
     * @return uint8_t - Status of calculations
     */
    uint8_t calculate_CRC(uint8_t* data, uint8_t length, uint8_t* result);

    /**
     * @brief Performs a soft reset on the MFRC522 chip and waits for it to be ready again.
     */
    void reset();

    /**
     * @brief Transmits SELECT/ANTICOLLISION commands to select a single PICC.
     * @details Before calling this function the PICCs must be placed in the READY(*) state by calling request() or
     * wake_up(). On success:
     *     - The chosen PICC is in state ACTIVE(*) and all other PICCs have returned to state IDLE/HALT. (Figure 7 of
     * the ISO/IEC 14443-3 draft.)
     *     - The UID size and value of the chosen PICC is returned in *uid along with the SAK.
     * @param uid structure containing uid of the card, size of the uid, SAK
     * @param valid_bits the number of known UID bits supplied in *uid. If set you must also supply uid->size. [default
     * value = 0]
     */
    uint8_t select_picc(UID* uid, uint8_t valid_bits = 0);

    /**
     * @brief Instructs a PICC in state ACTIVE(*) to go to state HALT.
     * @return uint8_t - Status of this action (_RFID_STATUS_OK on succes)
     */
    uint8_t halt();

  public:
    /**
     * @brief Construct a new RFID object
     * @details In case of setting RST pin perform hard reset or soft reset. Set proper speed values and set up
     * registers. At the end enable antena after either reset.
     */
    RFID();

    /**
     * @brief Check if card is present.
     */
    bool is_card();

    /**
     * @brief Read UID of the card.
     * @details Read the UID of placed card and return status of this reading. Only true on sucess.
     */
    bool read_card_serial();

    /**
     * @brief Return size of the card UID
     */
    uint8_t get_uid_size();

    /**
     * @brief Return array with UID.
     * @details Return UID array converted to decinals. Call halt() automaticly.
     */
    uint8_t* get_card_uid();

    /**
     * @brief Return a version of RFID card reader
     * @return uint8_t version byte
     */
    uint8_t version();

    /**
     * @brief Destroy the RFID object
     * @details Dummy desctructor
     */
    ~RFID(){};
};

#endif /* RFID_HH_ */
