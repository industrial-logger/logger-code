/**
 * @file sd_reg.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for SD Card Module
 * @version 0.1
 * @date 2022-01-10
 *
 * @copyright Copyright (C) 2009, ChaN
 *
 */

#ifndef SD_REG_HH_
#define SD_REG_HH_

#if(!defined _SD_DDR || !defined _SD_PORT)
#define _SD_DDR  DDRB
#define _SD_PORT PORTB
#define _SD_PIN  PINB
#endif

#if !defined _SD_CS
#define _SD_CS _SPI_CS_1
#endif

#define MMC_SEL !(PORTB & _BV(_SD_CS)) /* CS status (true:CS == L) */

#define STA_NOINIT 0x01 /* Drive not initialized */
#define STA_NODISK 0x02 /* No medium in the drive */

/* Card type flags (CardType) */
#define CT_MMC   0x01              /* MMC ver 3 */
#define CT_SD1   0x02              /* SD ver 1 */
#define CT_SD2   0x04              /* SD ver 2 */
#define CT_SDC   (CT_SD1 | CT_SD2) /* SD */
#define CT_BLOCK 0x08              /* Block addressing */

/* Definitions for MMC/SDC command */
#define CMD0   (0x40 + 0)  /* GO_IDLE_STATE */
#define CMD1   (0x40 + 1)  /* SEND_OP_COND (MMC) */
#define ACMD41 (0xC0 + 41) /* SEND_OP_COND (SDC) */
#define CMD8   (0x40 + 8)  /* SEND_IF_COND */
#define CMD16  (0x40 + 16) /* SET_BLOCKLEN */
#define CMD17  (0x40 + 17) /* READ_SINGLE_BLOCK */
#define CMD24  (0x40 + 24) /* WRITE_BLOCK */
#define CMD55  (0x40 + 55) /* APP_CMD */
#define CMD58  (0x40 + 58) /* READ_OCR */

/* Card type flags (CardType) */
#define CT_MMC   0x01 /* MMC ver 3 */
#define CT_SD1   0x02 /* SD ver 1 */
#define CT_SD2   0x04 /* SD ver 2 */
#define CT_BLOCK 0x08 /* Block addressing */

#endif /* SPI_REG_HH_ */
