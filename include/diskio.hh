/**
 * @file diskio.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for SD Card module. Low level disk interface module include file
 * @version 0.1
 * @date 2022-01-10
 *
 * @copyright Copyright (C) 2009, ChaN
 *
 */

#ifndef _DISKIO

#include "integer.hh"
#include "sd_reg.hh"

/* Status of Disk Functions */
typedef BYTE DSTATUS;

/* Results of Disk Functions */
typedef enum
{
    RES_OK = 0, /* 0: Function succeeded */
    RES_ERROR,  /* 1: Disk error */
    RES_NOTRDY, /* 2: Not ready */
    RES_PARERR  /* 3: Invalid parameter */
} DRESULT;

class DISKIO
{
  private:
    /**
     * @brief Enable SD chip for SPI communication. Set SD_CS pin low.
     */
    static void enable();

    /**
     * @brief Disable SD chip for SPI communication. Set SD_CS pin high.
     */
    static void disable();

    /**
     * @brief Send a command packet to MMC
     *
     * @param cmd 1st byte (Start + Index)
     * @param arg Argument (32 bits)
     * @return BYTE response value
     */
    static BYTE send_cmd(BYTE cmd, DWORD arg);

  public:
    static BYTE CardType;

    /**
     * @brief Construct a new DISKIO object
     *
     */
    DISKIO();

    /**
     * @brief Initialize Disk Drive
     *
     * @return DSTATUS Initialize result
     */
    static DSTATUS disk_initialize();

    /**
     * @brief Read partial sector
     *
     * @param buff Pointer to the read buffer
     * @param lba Sector number (LBA)
     * @param ofs Byte offset to read from (0..511)
     * @param cnt Number of bytes to read (ofs + cnt mus be <= 512)
     * @return DRESULT read result
     */
    static DRESULT disk_readp(BYTE*, DWORD, WORD, WORD);

    /**
     * @brief Write partial sector
     *
     * @param buff Pointer to the bytes to be written
     * @param sa Number of bytes to send, Sector number (LBA) or zero
     * @return DRESULT write result
     */
    static DRESULT disk_writep(const BYTE*, DWORD);
};

#define _DISKIO
#endif
