/**
 * @file interruptions.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for digital measurement module
 * @version 0.1
 * @date 2022-01-25
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#ifndef INTERRUPTIONS_HH_
#define INTERRUPTIONS_HH_

#if(!defined _SW_OK || !defined _SW_NOK)
#define _SW_OK  PD2
#define _SW_NOK PD3
#endif

class INTERRUPT
{
  private:
    /**
     * @brief
     *
     */
    static uint8_t ok;

    /**
     * @brief
     *
     */
    static uint8_t nok;

  public:
    /**
     * @brief Construct a new INTERRUPT object
     *
     */
    INTERRUPT();

    /**
     * @brief check status of ok
     *
     * @return true if ok is 1
     * @return false if ok is 0
     */
    bool is_ok() { return ok == 1; };

    /**
     * @brief check status ognok
     *
     * @return true if nok is 1
     * @return false if ok is 0
     */
    bool is_nok() { return nok == 1; };

    /**
     * @brief Set ok to 0
     *
     */
    void clear_ok() { ok = 0; };

    /**
     * @brief Set nok to 0
     *
     */
    void clear_nok() { nok = 0; };

    /**
     * @brief Set ok to 1
     *
     */
    static void set_ok() { ok = 1; };

    /**
     * @brief Set nok to 1
     *
     */
    static void set_nok() { nok = 1; };

    /**
     * @brief Destroy the INTERRUPT object
     *
     */
    ~INTERRUPT(){};
};

#endif /* INTERRUPTIONS_HH_ */
