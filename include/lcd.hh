/**
 * @file lcd.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for LCD module
 * @version 0.1
 * @date 2021-10-21
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <avr/io.h>
#include <util/delay.h>

#ifndef LCD_HH_
#define LCD_HH_

#if(!defined _LCD_DDR || !defined _LCD_PORT)
#define _LCD_DDR  DDRC
#define _LCD_PORT PORTC
#endif

#if(!defined _LCD_RS || !defined _LCD_EN)
#define _LCD_RS 0
#define _LCD_RW 1
#define _LCD_EN 2
#endif

#if(!defined _LCD_D4 || !defined _LCD_D5 || !defined _LCD_D6 || !defined _LCD_D7)
#define _LCD_D4 4
#define _LCD_D5 5
#define _LCD_D6 6
#define _LCD_D7 7
#endif

#define _LCD_COL_COUNT 16
#define _LCD_ROW_COUNT 2

#define _LCD_CLEAR_DISPLAY 0x01
#define _LCD_4BIT_MODE     0x02
#define _LCD_8BIT_MODE     0x10
#define _LCD_2LINE_4BIT    0x28
#define _LCD_2LINE_8BIT    0x38
#define _LCD_CURSOR_OFF    0x0C
#define _LCD_INCREMENT     0x06
#define _LCD_FIRST_LINE    0x80
#define _LCD_SECOND_LINE   0xC0
#define _LCD_SHIFT_RIGHT   0x1C
#define _LCD_SHIFT_LEFT    0x18

class LCD
{
  private:
    /**
     * @brief Send the command value to the LCD data port
     * @param cmd command value
     */
    void command(uint8_t cmd);

    /**
     * @brief Send command to the data port
     * @param sign Individual character from string
     */
    void write(uint8_t sign);

  public:
    /**
     * @brief Construct a new LCD object
     */
    LCD();

    /**
     * @brief Clear display and set cursor at home position
     */
    void clear();

    /**
     * @brief Shows the text on the display
     * @param string String with text
     */
    void display(uint8_t string[]);

    /**
     * @brief Set the position of text
     * @param y Number of row
     * @param x Number of column
     */
    void set_position(uint8_t y, uint8_t x);

    /**
     * @brief Moves text to the left
     */
    void shift_left();

    /**
     * @brief Moves text to the right
     */
    void shift_right();

    /**
     * @brief Move text forward and backward
     */
    void roll();

    /**
     * @brief Display text on selected position
     * @param y Number of row
     * @param x Number of column
     * @param string String with text
     */
    void display_on_pos(uint8_t y, uint8_t x, uint8_t string[]);

    /**
     * @brief Display text in hex on position
     * @param y Number of row
     * @param x Number of column
     * @param d Hex byte to display
     */
    void display_on_pos_hex(uint8_t y, uint8_t x, uint8_t d);

    /**
     * @brief Destroy the LCD object
     */
    ~LCD(){};
};

#endif /* LCD_HH_ */
