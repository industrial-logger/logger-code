/**
 * @file pwm.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for PWM module
 * @version 0.1
 * @date 2021-12-12
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <avr/io.h>
#include <util/delay.h>

#ifndef PWM_HH_
#define PWM_HH_

#if(!defined _PWM_PIN_VOLTAGE || !defined _PWM_DDR_VOLTAGE || !defined _PWM_PORT_VOLTAGE ||                            \
    !defined _PWM_REGISTRY_VOLTAGE)
#define _PWM_PIN_VOLTAGE      7
#define _PWM_DDR_VOLTAGE      DDRD
#define _PWM_PORT_VOLTAGE     PORTD
#define _PWM_REGISTRY_VOLTAGE OCR2
#endif

#if(!defined _PWM_PIN_CURRENT || !defined _PWM_DDR_CURRENT || !defined _PWM_PORT_CURRENT ||                            \
    !defined _PWM_REGISTRY_CURRENT)
#define _PWM_PIN_CURRENT      3
#define _PWM_DDR_CURRENT      DDRB
#define _PWM_PORT_CURRENT     PORTB
#define _PWM_REGISTRY_CURRENT OCR0
#endif

class PWM
{
  private:
    /**
     * @brief Value of maximum current duty
     */
    const uint8_t max_current_duty = 155;

    /**
     * @brief Value of maximum voltage duty
     */
    const uint8_t max_voltage_duty = 183;

  public:
    /**
     * @brief Construct a new PWM object
     */
    PWM();

    /**
     * @brief Set the voltage value
     * @param voltage value of the voltage in range 0-10V
     */
    void set_voltage(float voltage);

    /**
     * @brief Set the current value
     * @param current value of the current in range 4-20mA
     */
    void set_current(float current);

    /**
     * @brief Destroy the PWM object
     * @details Dummy desctructor.
     */
    ~PWM(){};
};

#endif /* PWM_HH_ */
