/**
 * @file led.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for LED module
 * @version 0.1
 * @date 2021-10-21
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <avr/io.h>
#include <util/delay.h>

#ifndef LED_HH_
#define LED_HH_

#if(!defined _LED_DDR || !defined _LED_PORT)
#define _LED_DDR DDRA
#define _LED_PORT PORTA
#endif

#if(!defined _LED_RED || !defined _LED_GREEN || !defined _LED_YELLOW)
#define _LED_RED 5
#define _LED_YELLOW 6
#define _LED_GREEN 7
#endif

class LED
{
  private:
  public:
    /**
     * @brief Construct a new LED object
     */
    LED();

    /**
     * @brief Turn on specific LED
     * @param led_pin LEDs pin number
     */
    void turn_on(int led_pin);

    /**
     * @brief Turn off specific LED
     * @param led_pin LEDs pin number
     */
    void turn_off(int led_pin);

    /**
     * @brief Destroy the LED object
     */
    ~LED(){};
};

#endif /* LED_HH_ */