/**
 * @file spi.hh
 * @authors Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for SPI module
 * @version 0.1
 * @date 2021-10-21
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <avr/io.h>
#include <util/delay.h>

#ifndef SPI_HH_
#define SPI_HH_

#define _SPI_DDR  DDRB
#define _SPI_PORT PORTB

#define _SPI_SS   4
#define _SPI_MOSI 5
#define _SPI_MISO 6
#define _SPI_SCK  7

#if(!defined _SPI_CS_1 || !defined _SPI_CS_2)
#define _SPI_CS_1 0
#define _SPI_CS_2 1
#endif

class SPI
{
  public:
    /**
     * @brief Construct a new SPI object
     * @details Set directions on all pins and initialize them with proper
     * value. Enable SPI communication in master mode with speed of Fosc/2
     */
    SPI();

    /**
     * @brief Send data to slave device
     * @param data Data to be transmitted
     * @return uint8_t - Value of SPDR register
     */
    static uint8_t master_transmit(uint8_t data);

    /**
     * @brief Read data from slave device
     * @return uint8_t - Value of SPDR register
     */
    static uint8_t master_read();

    /**
     * @brief Destroy the SPI object
     * @details Dummy descructor
     */
    ~SPI(){};
};

#endif /* SPI_HH_ */
