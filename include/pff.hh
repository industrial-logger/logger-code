/*---------------------------------------------------------------------------/
/  Petit FatFs - FAT file system module include file  R0.02a   (C)ChaN, 2010
/----------------------------------------------------------------------------/
/ Petit FatFs module is an open source software to implement FAT file system to
/ small embedded systems. This is a free software and is opened for education,
/ research and commercial developments under license policy of following trems.
/
/  Copyright (C) 2010, ChaN, all right reserved.
/
/ * The Petit FatFs module is a free software and there is NO WARRANTY.
/ * No restriction on use. You can use, modify and redistribute it for
/   personal, non-profit or commercial use UNDER YOUR RESPONSIBILITY.
/ * Redistributions of source code must retain the above copyright notice.
/
/----------------------------------------------------------------------------*/

/**
 * @file pff.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for PFF module
 * @version 0.1
 * @date 2022-01-10
 *
 * @copyright Copyright (C) 2010, ChaN
 *
 */

#include "diskio.hh"

/*---------------------------------------------------------------------------/
/ Petit FatFs Configuration Options
/
/ CAUTION! Do not forget to make clean the project after any changes to
/ the configuration options.
/
/----------------------------------------------------------------------------*/
#ifndef _FATFS
#define _FATFS

#define _USE_READ  1 /* 1:Enable read() */
#define _USE_DIR   0 /* 1:Enable opendir() and readdir() */
#define _USE_LSEEK 0 /* 1:Enable lseek() */
#define _USE_WRITE 0 /* 1:Enable write() */
#define _FS_FAT12  0 /* 1:Enable FAT12 support */
#define _FS_FAT32  1 /* 1:Enable FAT32 support */
#define _CODE_PAGE 1
/* Defines which code page is used for path name. Supported code pages are:
/  932, 936, 949, 950, 437, 720, 737, 775, 850, 852, 855, 857, 858, 862, 866,
/  874, 1250, 1251, 1252, 1253, 1254, 1255, 1257, 1258 and 1 (ASCII only).
/  SBCS code pages except for 1 requiers a case conversion table. This
/  might occupy 128 bytes on the RAM on some platforms, e.g. avr-gcc. */

#define _WORD_ACCESS 0
/* The _WORD_ACCESS option defines which access method is used to the word
/  data in the FAT structure.
/
/   0: Byte-by-byte access. Always compatible with all platforms.
/   1: Word access. Do not choose this unless following condition is met.
/
/  When the byte order on the memory is big-endian or address miss-aligned
/  word access results incorrect behavior, the _WORD_ACCESS must be set to 0.
/  If it is not the case, the value can also be set to 1 to improve the
/  performance and code efficiency. */

/* End of configuration options. Do not change followings without care.     */
/*--------------------------------------------------------------------------*/

#if _FS_FAT32
#define CLUST DWORD
#else
#define CLUST WORD
#endif

/* File system object structure */

typedef struct
{
    BYTE fs_type; /* FAT sub type */
    BYTE flag;    /* File status flags */
    BYTE csize;   /* Number of sectors per cluster */
    BYTE pad1;
    WORD n_rootdir;   /* Number of root directory entries (0 on FAT32) */
    CLUST n_fatent;   /* Number of FAT entries (= number of clusters + 2) */
    DWORD fatbase;    /* FAT start sector */
    DWORD dirbase;    /* Root directory start sector (Cluster# on FAT32) */
    DWORD database;   /* Data start sector */
    DWORD fptr;       /* File R/W pointer */
    DWORD fsize;      /* File size */
    CLUST org_clust;  /* File start cluster */
    CLUST curr_clust; /* File current cluster */
    DWORD dsect;      /* File current data sector */
} FATFS;

/* Directory object structure */

typedef struct
{
    WORD index;   /* Current read/write index number */
    BYTE* fn;     /* Pointer to the SFN (in/out) {file[8],ext[3],status[1]} */
    CLUST sclust; /* Table start cluster (0:Static table) */
    CLUST clust;  /* Current cluster */
    DWORD sect;   /* Current sector */
} DIR;

/* File status structure */

typedef struct
{
    DWORD fsize;    /* File size */
    WORD fdate;     /* Last modified date */
    WORD ftime;     /* Last modified time */
    BYTE fattrib;   /* Attribute */
    char fname[13]; /* File name */
} FILINFO;

/* File function return code (FRESULT) */

typedef enum
{
    FR_OK = 0,       /* 0 */
    FR_DISK_ERR,     /* 1 */
    FR_NOT_READY,    /* 2 */
    FR_NO_FILE,      /* 3 */
    FR_NO_PATH,      /* 4 */
    FR_NOT_OPENED,   /* 5 */
    FR_NOT_ENABLED,  /* 6 */
    FR_NO_FILESYSTEM /* 7 */
} FRESULT;

class PFF
{
  private:
    /**
     * @brief Pointer to the file system object (logical drive)
     *
     */
    static FATFS* FatFs;

    /**
     * @brief Fill memory
     *
     * @param dst pointer to the destination object
     * @param val value
     * @param cnt number of bytes to copy
     */
    static void mem_set(void* dst, int val, int cnt);

    /**
     * @brief Compare memory to memory
     *
     * @param dst pointer to the destination object
     * @param src pointer to the source object
     * @param cnt number of bytes to copy
     * @return int the pointer to the destination buffer
     */
    static int mem_cmp(const void* dst, const void* src, int cnt);

    /**
     * @brief FAT access - Read value of a FAT entry
     *
     * @param clst Cluster# to get the link information
     * @return CLUST 1:IO error, Else:Cluster status
     */

    static CLUST get_fat(CLUST clst);
    /**
     * @brief Get sector# from cluster#
     *
     * @param clst Cluster# to be converted
     * @return DWORD !=0: Sector number, 0: Failed - invalid cluster#
     */
    static DWORD clust2sect(CLUST clst);

    /**
     * @brief Directory handling - Rewind directory index
     *
     * @param dj Pointer to directory object
     * @return FRESULT Seek result
     */
    static FRESULT dir_rewind(DIR* dj);

    /**
     * @brief Directory handling - Move directory index next
     *
     * @param dj Pointer to directory object
     * @return FRESULT FR_OK:Succeeded, FR_NO_FILE:End of table
     */
    static FRESULT dir_next(DIR* dj);

    /**
     * @brief Directory handling - Find an object in the directory
     *
     * @param dj Pointer to the directory object linked to the file name
     * @param dir 32-byte working buffer
     * @return FRESULT result
     */
    static FRESULT dir_find(DIR* dj, BYTE* dir);

    /**
     * @brief Pick a segment and create the object name in directory form
     *
     * @param dj Pointer to the directory object
     * @param path Pointer to pointer to the segment in the path string
     * @return FRESULT result of create the object name
     */
    static FRESULT create_name(DIR* dj, const char** path);

    /**
     * @brief Follow a file path
     *
     * @param dj Directory object to return last directory and found object
     * @param dir 32-byte working buffer
     * @param path Full-path string to find a file or directory
     * @return FRESULT FR_OK(0): successful, !=0: error code
     */
    static FRESULT follow_path(DIR* dj, BYTE* dir, const char* path);

    /**
     * @brief Check a sector if it is an FAT boot record
     *
     * @param buf Working buffer
     * @param sect Sector# (lba) to check if it is an FAT boot record or not
     * @return BYTE 0:The FAT boot record, 1:Valid boot record but not an FAT, 2:Not a boot record, 3:Error
     */
    static BYTE check_fs(BYTE* buf, DWORD sect);

  public:
    /**
     * @brief Construct a new PFF object
     *
     */
    PFF();

    /**
     * @brief Mount/Unmount a logical drive
     *
     * @param fs Pointer to new file system object description
     * @return FRESULT
     */
    FRESULT mount(FATFS* fs);

    /**
     * @brief Open a file
     *
     * @param path Pointer to the file name
     * @return FRESULT open result
     */
    FRESULT open(const char* path);

    /**
     * @brief Read data from the open file
     *
     * @param buff Pointer to the read buffer
     * @param btr Number of bytes to read
     * @param br Pointer to number of bytes read
     * @return FRESULT read result
     */
    FRESULT read(void* buff, WORD btr, WORD* br);

    /**
     * @brief Write data to the open file
     *
     * @param buff Pointer to the data to be written
     * @param btw Number of bytes to write (0:Finalize the current write operation)
     * @param bw Pointer to number of bytes written
     * @return FRESULT write result
     */
    FRESULT write(const void* buff, WORD btw, WORD* bw);

    /**
     * @brief Move file pointer of the open file
     *
     * @param ofs File pointer from top of fil
     * @return FRESULT lseek result
     */
    FRESULT lseek(DWORD ofs);
};

#endif /* _FATFS */
