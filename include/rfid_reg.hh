/**
 * @file rfid_reg.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Header file for RFID registers
 * @version 0.1
 * @date 2021-11-10
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef RFID_REG_HH_
#define RFID_REG_HH_

/*
 * MFRC522 registers; described in chapter 9 of the datasheet
 */
// PAGE 0: Command and status
//                           0x00 // reserved for future use
#define _RFID_COMMAND_REG    0X01 // starts and stops command execution
#define _RFID_COMIEN_REG     0X02 // enable and disable interrupt request control bits
#define _RFID_DIVIEN_REG     0X03 // enable and disable interrupt request control bits
#define _RFID_COMIRQ_REG     0X04 // interrupt request bits
#define _RFID_DIVIRQ_REG     0X05 // interrupt request bits
#define _RFID_ERROR_REG      0X06 // error bits showing the error status of the last command executed
#define _RFID_STATUS1_REG    0X07 // communication status bits
#define _RFID_STATUS2_REG    0X08 // receiver and transmitter status bits
#define _RFID_FIFODATA_REG   0X09 // input and output of 64 byte FIFO buffer
#define _RFID_FIFOLEVEL_REG  0X0A // number of bytes stored in the FIFO buffer
#define _RFID_WATERLEVEL_REG 0X0B // level for FIFO underflow and overflow warning
#define _RFID_CONTROL_REG    0X0C // miscellaneous control registers
#define _RFID_BITFRAMING_REG 0X0D // adjustments for bit-oriented frames
#define _RFID_COLL_REG       0X0E // bit position of the first bit-collision detected on the RF interface
//                           0X0F // reserved for future use

// PAGE 1: Command
//                            0X10 // reserved for future use
#define _RFID_MODE_REG        0X11 // defines general modes for transmitting and receiving
#define _RFID_TXMODE_REG      0X12 // defines transmission data rate and framing
#define _RFID_RXMODE_REG      0X13 // defines reception data rate and framing
#define _RFID_TXCONTROL_REG   0X14 // controls the logical behavior of the antenna driver pins TX1 and TX2
#define _RFID_TXASK_REG       0X15 // controls the setting of the transmission modulation
#define _RFID_TXSEL_REG       0X16 // selects the internal sources for the antenna driver
#define _RFID_RXSEL_REG       0X17 // selects internal receiver settings
#define _RFID_RXTHRESHOLD_REG 0X18 // selects thresholds for the bit decoder
#define _RFID_DEMOD_REG       0X19 // defines demodulator settings
//                            0X1A // reserved for future use
//                            0X1B // reserved for future use
#define _RFID_MFTX_REG        0X1C // controls some MIFARE communication transmit parameters
#define _RFID_MFRX_REG        0X1D // controls some MIFARE communication receive parameters
//                            0X1E // reserved for future use
#define _RFID_SERIALSPEED_REG 0X1F // selects the speed of the serial UART interface

// PAGE 2: Configuration
//                              0X20 // reserved for future use
#define _RFID_CRCRESULT_REG_M   0X21 // shows the MSB and LSB values of the CRC calculation
#define _RFID_CRCRESULT_REG_L   0X22
//                              0X23 // reserved for future use
#define _RFID_MODWIDTH_REG      0X24 // controls the ModWidth setting?
//                              0X25 // reserved for future use
#define _RFID_RFCFG_REG         0X26 // configures the receiver gain
#define _RFID_GSN_REG           0X27 // selects the conductance of the antenna driver pins TX1 and TX2 for modulation
#define _RFID_CWGSP_REG         0X28 // defines the conductance of the p-driver output during periods of no modulation
#define _RFID_MODGSP_REG        0X29 // defines the conductance of the p-driver output during periods of modulation
#define _RFID_TMODE_REG         0X2A // defines settings for the internal timer
#define _RFID_TPRESCALER_REG    0X2B // the lower 8 bits of the TPrescaler value. The 4 high bits are in TModeReg.
#define _RFID_TRELOAD_REG_H     0X2C // defines the 16-bit timer reload value
#define _RFID_TRELOAD_REG_L     0X2D
#define _RFID_TCOUNTERVAL_REG_1 0X2E // shows the 16-bit timer value
#define _RFID_TCOUNTERVAL_REG_2 0X2F

// PAGE 3: Test register
//                             0x30 // reserved for future use
#define _RFID_TESTSEL1_REG     0X31 // general test signal configuration
#define _RFID_TESTSEL2_REG     0X32 // general test signal configuration
#define _RFID_TESTPINEN_REG    0X33 // enables pin output driver on pins D1 to D7
#define _RFID_TESTPINVALUE_REG 0X34 // defines the values for D1 to D7 when it is used as an I/O bus
#define _RFID_TESTBUS_REG      0X35 // shows the status of the internal test bus
#define _RFID_AUTOTEST_REG     0X36 // controls the digital self-test
#define _RFID_VERSION_REG      0X37 // shows the software version
#define _RFID_ANALOGTEST_REG   0X38 // controls the pins AUX1 and AUX2
#define _RFID_TESTDAC1_REG     0X39 // defines the test value for TestDAC1
#define _RFID_TESTDAC2_REG     0X3A // defines the test value for TestDAC2
#define _RFID_TESTADC_REG      0X3B // shows the value of ADC I and Q channels
//                             0x3C // reserved for production tests
//                             0x3D // reserved for production tests
//                             0x3E // reserved for production tests
//                             0x3F // reserved for production tests

/*
 * MFRC522 commands. Described in chapter 10 of the datasheet.
 */
#define _RFID_PCD_IDLE               0x00 // no action, cancels current command execution
#define _RFID_PCD_MEM                0x01 // stores 25 bytes into the internal buffer
#define _RFID_PCD_GENERATE_RANDOM_ID 0x02 // generates a 10-byte random ID number
#define _RFID_PCD_CALC_CRC           0x03 // activates the CRC coprocessor or performs a self-test
#define _RFID_PCD_TRANSMIT           0x04 // transmits data from the FIFO buffer
#define _RFID_PCD_NO_CMD_CHANGE      0x07 // no command change
#define _RFID_PCD_RECEIVE            0x08 // activates the receiver circuits
#define _RFID_PCD_TRANSCEIVE         0x0C // transmits data from FIFO buffer to antenna
#define _RFID_PCD_AUTHENT            0x0E // performs the MIFARE standard authentication as a reader
#define _RFID_PCD_SOFT_RESET         0x0F // resets the MFRC522

/*
 * Commands sent to the PICC
 * The commands used by the PCD to manage communication with several PICCs (ISO 14443-3, Type A, section 6.4)
 */
#define _RFID_PICC_CMD_REQA          0x26 // REQuest command, Type A.
#define _RFID_PICC_CMD_WUPA          0x52 // Wake-UP command, Type A.
#define _RFID_PICC_CMD_CT            0x88 // Cascade Tag. Not really a command, but used during anti collision.
#define _RFID_PICC_CMD_SEL_CL1       0x93 // Anti collision/Select, Cascade Level 1
#define _RFID_PICC_CMD_SEL_CL2       0x95 // Anti collision/Select, Cascade Level 2
#define _RFID_PICC_CMD_SEL_CL3       0x97 // Anti collision/Select, Cascade Level 3
#define _RFID_PICC_CMD_HLTA          0x50 // HaLT command, Type A. Instructs an ACTIVE PICC to go to state HALT.
#define _RFID_PICC_CMD_RATS          0xE0 // Request command for Answer To Reset.
#define _RFID_PICC_CMD_MF_AUTH_KEY_A 0x60 // Perform authentication with Key A
#define _RFID_PICC_CMD_MF_AUTH_KEY_B 0x61 // Perform authentication with Key B
#define _RFID_PICC_CMD_MF_READ       0x30 // Reads one 16 byte block from the authenticated sector of the PICC.
#define _RFID_PICC_CMD_MF_WRITE      0xA0 // Writes one 16 byte block to the authenticated sector of the PICC.
#define _RFID_PICC_CMD_MF_DECREMENT  0xC0 // Decrements contents of a block; stores result in internal data register.
#define _RFID_PICC_CMD_MF_INCREMENT  0xC1 // Increments contents of a block; stores result in internal data register.
#define _RFID_PICC_CMD_MF_RESTORE    0xC2 // Reads the contents of a block into the internal data register.
#define _RFID_PICC_CMD_MF_TRANSFER   0xB0 // Writes the contents of the internal data register to a block.
#define _RFID_PICC_CMD_UL_WRITE      0xA2 // Writes one 4 byte page to the PICC.

/*
 * Return codes from the functions in RFID class
 */
#define _RFID_STATUS_OK             1  // Success
#define _RFID_STATUS_ERROR          2  // Error in communication
#define _RFID_STATUS_COLLISION      3  // Collision detected
#define _RFID_STATUS_TIMEOUT        4  // Timeout in communication
#define _RFID_STATUS_NO_ROOM        5  // A buffer is not big enough
#define _RFID_STATUS_INTERNAL_ERROR 6  // Internal error in the code. Should not happen ;-)
#define _RFID_STATUS_INVALID        7  // Invalid argument
#define _RFID_STATUS_CRC_WRONG      8  // The CRC_A does not match
#define _RFID_STATUS_UNKNOWN        9  //
#define _RFID_STATUS_MIFARE_NACK    10 // A MIFARE PICC responded with NAK

#endif /* RFID_REG_HH_ */
