# Logger code

## Requirements

In order to compile binary you need to have this programs installed on your
system:

- `cmake`
- `make`
- `avr-gcc`
- `avr-libc`
- `avr-binutils`
- `avrdude`

In some cases you will also need to install `gcc`.
