/*----------------------------------------------------------------------------/
/  Petit FatFs - FAT file system module  R0.02a                (C)ChaN, 2010
/-----------------------------------------------------------------------------/
/ Petit FatFs module is an open source software to implement FAT file system to
/ small embedded systems. This is a free software and is opened for education,
/ research and commercial developments under license policy of following trems.
/
/  Copyright (C) 2010, ChaN, all right reserved.
/
/ * The Petit FatFs module is a free software and there is NO WARRANTY.
/ * No restriction on use. You can use, modify and redistribute it for
/   personal, non-profit or commercial use UNDER YOUR RESPONSIBILITY.
/ * Redistributions of source code must retain the above copyright notice.
/
/-----------------------------------------------------------------------------/
/ Jun 15,'09  R0.01a  First release. (Branched from FatFs R0.07b.)
/
/ Dec 14,'09  R0.02   Added multiple code page support.
/                     Added write funciton.
/                     Changed stream read mode interface.
/ Dec 07,'10  R0.02a  Added some configuration options.
/                     Fixed fails to open objects with DBCS character.
/----------------------------------------------------------------------------*/

/**
 * @file pff.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Source file for PFF module
 * @version 0.1
 * @date 2022-01-10
 *
 * @copyright Copyright (C) 2010, ChaN
 *
 */

#include "pff.hh"
#include "pff_reg.hh"

#ifdef _EXCVT
static const BYTE cvt[] = _EXCVT;
#endif

FATFS* PFF::FatFs;

void PFF::mem_set(void* dst, int val, int cnt)
{
    char* d = (char*)dst;
    while(cnt--) *d++ = (char)val;
}

int PFF::mem_cmp(const void* dst, const void* src, int cnt)
{
    const char *d = (const char*)dst, *s = (const char*)src;
    int r = 0;
    while(cnt-- && (r = *d++ - *s++) == 0)
        ;
    return r;
}

CLUST PFF::get_fat(CLUST clst)
{

    BYTE buf[4];
    FATFS* fs = FatFs;

    if(clst < 2 || clst >= fs->n_fatent) /* Range check */
        return 1;

    switch(fs->fs_type)
    {
#if _FS_FAT12
        case FS_FAT12:
            bc = (WORD)clst;
            bc += bc / 2;
            ofs = bc % 512;
            bc /= 512;
            if(ofs != 511)
            {
                if(disk_readp(buf, fs->fatbase + bc, ofs, 2)) break;
            }
            else
            {
                if(disk_readp(buf, fs->fatbase + bc, 511, 1)) break;
                if(disk_readp(buf + 1, fs->fatbase + bc + 1, 0, 1)) break;
            }
            wc = LD_WORD(buf);
            return (clst & 1) ? (wc >> 4) : (wc & 0xFFF);
#endif
        case FS_FAT16:
            if(DISKIO::disk_readp(buf, fs->fatbase + clst / 256, (WORD)(((WORD)clst % 256) * 2), 2)) break;
            return LD_WORD(buf);
#if _FS_FAT32
        case FS_FAT32:
            if(DISKIO::disk_readp(buf, fs->fatbase + clst / 128, (WORD)(((WORD)clst % 128) * 4), 4)) break;
            return LD_DWORD(buf) & 0x0FFFFFFF;
#endif
    }

    return 1; /* An error occured at the disk I/O layer */
}

DWORD PFF::clust2sect(CLUST clst)
{
    FATFS* fs = FatFs;

    clst -= 2;
    if(clst >= (fs->n_fatent - 2)) return 0; /* Invalid cluster# */
    return (DWORD)clst * fs->csize + fs->database;
}

FRESULT PFF::dir_rewind(DIR* dj)
{
    CLUST clst;
    FATFS* fs = FatFs;

    dj->index = 0;
    clst = dj->sclust;
    if(clst == 1 || clst >= fs->n_fatent) /* Check start cluster range */
        return FR_DISK_ERR;
    if(_FS_FAT32 && !clst && fs->fs_type == FS_FAT32) /* Replace cluster# 0 with root cluster# if in FAT32 */
        clst = (CLUST)fs->dirbase;
    dj->clust = clst;                                 /* Current cluster */
    dj->sect = clst ? clust2sect(clst) : fs->dirbase; /* Current sector */

    return FR_OK; /* Seek succeeded */
}

FRESULT PFF::dir_next(DIR* dj)
{
    CLUST clst;
    WORD i;
    FATFS* fs = FatFs;

    i = dj->index + 1;
    if(!i || !dj->sect) /* Report EOT when index has reached 65535 */
        return FR_NO_FILE;

    if(!(i % 16))
    {               /* Sector changed? */
        dj->sect++; /* Next sector */

        if(dj->clust == 0)
        {                          /* Static table */
            if(i >= fs->n_rootdir) /* Report EOT when end of table */
                return FR_NO_FILE;
        }
        else
        { /* Dynamic table */
            if(((i / 16) & (fs->csize - 1)) == 0)
            {                              /* Cluster changed? */
                clst = get_fat(dj->clust); /* Get next cluster */
                if(clst <= 1) return FR_DISK_ERR;
                if(clst >= fs->n_fatent) /* When it reached end of dynamic table */
                    return FR_NO_FILE;   /* Report EOT */
                dj->clust = clst;        /* Initialize data for new cluster */
                dj->sect = clust2sect(clst);
            }
        }
    }

    dj->index = i;

    return FR_OK;
}

FRESULT PFF::dir_find(DIR* dj, BYTE* dir)
{
    FRESULT res;
    BYTE c;

    res = dir_rewind(dj); /* Rewind directory object */
    if(res != FR_OK) return res;

    do
    {
        res = DISKIO::disk_readp(dir, dj->sect, (WORD)((dj->index % 16) * 32), 32) /* Read an entry */
                  ? FR_DISK_ERR
                  : FR_OK;
        if(res != FR_OK) break;
        c = dir[DIR_Name]; /* First character */
        if(c == 0)
        {
            res = FR_NO_FILE;
            break;
        }                                                          /* Reached to end of table */
        if(!(dir[DIR_Attr] & AM_VOL) && !mem_cmp(dir, dj->fn, 11)) /* Is it a valid entry? */
            break;
        res = dir_next(dj); /* Next entry */
    } while(res == FR_OK);

    return res;
}

FRESULT PFF::create_name(DIR* dj, const char** path)
{
    BYTE c, d, ni, si, i, *sfn;
    const char* p;

    /* Create file name in directory form */
    sfn = dj->fn;
    mem_set(sfn, ' ', 11);
    si = i = 0;
    ni = 8;
    p = *path;
    for(;;)
    {
        c = p[si++];
        if(c <= ' ' || c == '/') break; /* Break on end of segment */
        if(c == '.' || i >= ni)
        {
            if(ni != 8 || c != '.') break;
            i = 8;
            ni = 11;
            continue;
        }
#ifdef _EXCVT
        if(c >= 0x80) /* To upper extended char (SBCS) */
            c = cvt[c - 0x80];
#endif
        if(IsDBCS1(c) && i < ni - 1)
        {                /* DBC 1st byte? */
            d = p[si++]; /* Get 2nd byte */
            sfn[i++] = c;
            sfn[i++] = d;
        }
        else
        {                             /* Single byte code */
            if(IsLower(c)) c -= 0x20; /* toupper */
            sfn[i++] = c;
        }
    }
    *path = &p[si]; /* Rerurn pointer to the next segment */

    sfn[11] = (c <= ' ') ? 1 : 0; /* Set last segment flag if end of path */

    return FR_OK;
}

FRESULT PFF::follow_path(DIR* dj, BYTE* dir, const char* path)
{
    FRESULT res;

    while(*path == ' ') path++; /* Skip leading spaces */
    if(*path == '/') path++;    /* Strip heading separator */
    dj->sclust = 0;             /* Set start directory (always root dir) */

    if((BYTE)*path <= ' ')
    { /* Null path means the root directory */
        res = dir_rewind(dj);
        dir[0] = 0;
    }
    else
    { /* Follow path */
        for(;;)
        {
            res = create_name(dj, &path); /* Get a segment */
            if(res != FR_OK) break;
            res = dir_find(dj, dir); /* Find it */
            if(res != FR_OK)
            { /* Could not find the object */
                if(res == FR_NO_FILE && !*(dj->fn + 11)) res = FR_NO_PATH;
                break;
            }
            if(*(dj->fn + 11)) break; /* Last segment match. Function completed. */
            if(!(dir[DIR_Attr] & AM_DIR))
            { /* Cannot follow because it is a file */
                res = FR_NO_PATH;
                break;
            }
            dj->sclust = LD_CLUST(dir);
        }
    }

    return res;
}

BYTE PFF::check_fs(BYTE* buf, DWORD sect)
{
    if(DISKIO::disk_readp(buf, sect, 510, 2)) /* Read the boot sector */
        return 3;
    if(LD_WORD(buf) != 0xAA55) /* Check record signature */
        return 2;

    if(!DISKIO::disk_readp(buf, sect, BS_FilSysType, 2) && LD_WORD(buf) == 0x4146) /* Check FAT12/16 */
        return 0;
    if(_FS_FAT32 && !DISKIO::disk_readp(buf, sect, BS_FilSysType32, 2) && LD_WORD(buf) == 0x4146) /* Check FAT32 */
        return 0;
    return 1;
}

FRESULT PFF::mount(FATFS* fs)
{
    BYTE fmt, buf[36];
    DWORD bsect, fsize, tsect, mclst;

    FatFs = 0;
    if(!fs) return FR_OK; /* Unregister fs object */

    if(DISKIO::disk_initialize() & STA_NOINIT) /* Check if the drive is ready or not */
        return FR_NOT_READY;

    /* Search FAT partition on the drive */
    bsect = 0;
    fmt = check_fs(buf, bsect); /* Check sector 0 as an SFD format */
    if(fmt == 1)
    { /* Not an FAT boot record, it may be FDISK format */
        /* Check a partition listed in top of the partition table */
        if(DISKIO::disk_readp(buf, bsect, MBR_Table, 16))
        { /* 1st partition entry */
            fmt = 3;
        }
        else
        {
            if(buf[4])
            {                               /* Is the partition existing? */
                bsect = LD_DWORD(&buf[8]);  /* Partition offset in LBA */
                fmt = check_fs(buf, bsect); /* Check the partition */
            }
        }
    }
    if(fmt == 3) return FR_DISK_ERR;
    if(fmt) return FR_NO_FILESYSTEM; /* No valid FAT patition is found */

    /* Initialize the file system object */
    if(DISKIO::disk_readp(buf, bsect, 13, sizeof(buf))) return FR_DISK_ERR;

    fsize = LD_WORD(buf + BPB_FATSz16 - 13); /* Number of sectors per FAT */
    if(!fsize) fsize = LD_DWORD(buf + BPB_FATSz32 - 13);

    fsize *= buf[BPB_NumFATs - 13];                           /* Number of sectors in FAT area */
    fs->fatbase = bsect + LD_WORD(buf + BPB_RsvdSecCnt - 13); /* FAT start sector (lba) */
    fs->csize = buf[BPB_SecPerClus - 13];                     /* Number of sectors per cluster */
    fs->n_rootdir = LD_WORD(buf + BPB_RootEntCnt - 13);       /* Nmuber of root directory entries */
    tsect = LD_WORD(buf + BPB_TotSec16 - 13);                 /* Number of sectors on the file system */
    if(!tsect) tsect = LD_DWORD(buf + BPB_TotSec32 - 13);
    mclst = (tsect /* Last cluster# + 1 */
             - LD_WORD(buf + BPB_RsvdSecCnt - 13) - fsize - fs->n_rootdir / 16) /
                fs->csize +
            2;
    fs->n_fatent = (CLUST)mclst;

    fmt = FS_FAT16;   /* Determine the FAT sub type */
    if(mclst < 0xFF7) /* Number of clusters < 0xFF5 */
#if _FS_FAT12
        fmt = FS_FAT12;
#else
        return FR_NO_FILESYSTEM;
#endif
    if(mclst >= 0xFFF7) /* Number of clusters >= 0xFFF5 */
#if _FS_FAT32
        fmt = FS_FAT32;
#else
        return FR_NO_FILESYSTEM;
#endif

    fs->fs_type = fmt; /* FAT sub-type */
    if(_FS_FAT32 && fmt == FS_FAT32)
        fs->dirbase = LD_DWORD(buf + (BPB_RootClus - 13)); /* Root directory start cluster */
    else
        fs->dirbase = fs->fatbase + fsize;                   /* Root directory start sector (lba) */
    fs->database = fs->fatbase + fsize + fs->n_rootdir / 16; /* Data start sector (lba) */

    fs->flag = 0;
    FatFs = fs;

    return FR_OK;
}

FRESULT PFF::open(const char* path)
{
    FRESULT res;
    DIR dj;
    BYTE sp[12], dir[32];
    FATFS* fs = FatFs;

    if(!fs) /* Check file system */
        return FR_NOT_ENABLED;

    fs->flag = 0;
    dj.fn = sp;
    res = follow_path(&dj, dir, path);      /* Follow the file path */
    if(res != FR_OK) return res;            /* Follow failed */
    if(!dir[0] || (dir[DIR_Attr] & AM_DIR)) /* It is a directory */
        return FR_NO_FILE;

    fs->org_clust = LD_CLUST(dir);            /* File start cluster */
    fs->fsize = LD_DWORD(dir + DIR_FileSize); /* File size */
    fs->fptr = 0;                             /* File pointer */
    fs->flag = FA_OPENED;

    return FR_OK;
}

FRESULT PFF::read(void* buff, WORD btr, WORD* br)
{
    DRESULT dr;
    CLUST clst;
    DWORD sect, remain;
    WORD rcnt;
    BYTE cs, *rbuff = (BYTE*)buff;
    FATFS* fs = FatFs;

    *br = 0;
    if(!fs) return FR_NOT_ENABLED; /* Check file system */
    if(!(fs->flag & FA_OPENED))    /* Check if opened */
        return FR_NOT_OPENED;

    remain = fs->fsize - fs->fptr;
    if(btr > remain) btr = (WORD)remain; /* Truncate btr by remaining bytes */

    while(btr)
    { /* Repeat until all data transferred */
        if((fs->fptr % 512) == 0)
        {                                                  /* On the sector boundary? */
            cs = (BYTE)(fs->fptr / 512 & (fs->csize - 1)); /* Sector offset in the cluster */
            if(!cs)
            {                            /* On the cluster boundary? */
                clst = (fs->fptr == 0) ? /* On the top of the file? */
                           fs->org_clust
                                       : get_fat(fs->curr_clust);
                if(clst <= 1) goto fr_abort;
                fs->curr_clust = clst; /* Update current cluster */
            }
            sect = clust2sect(fs->curr_clust); /* Get current sector */
            if(!sect) goto fr_abort;
            fs->dsect = sect + cs;
        }
        rcnt = (WORD)(512 - (fs->fptr % 512)); /* Get partial sector data from sector buffer */
        if(rcnt > btr) rcnt = btr;
        dr = DISKIO::disk_readp(!buff ? 0 : rbuff, fs->dsect, (WORD)(fs->fptr % 512), rcnt);
        if(dr) goto fr_abort;
        fs->fptr += rcnt;
        rbuff += rcnt; /* Update pointers and counters */
        btr -= rcnt;
        *br += rcnt;
    }

    return FR_OK;

fr_abort:
    fs->flag = 0;
    return FR_DISK_ERR;
}

FRESULT PFF::write(const void* buff, WORD btw, WORD* bw)
{
    CLUST clst;
    DWORD sect, remain;
    const BYTE* p = (const BYTE*)buff;
    BYTE cs;
    WORD wcnt;
    FATFS* fs = FatFs;

    *bw = 0;
    if(!fs) return FR_NOT_ENABLED; /* Check file system */
    if(!(fs->flag & FA_OPENED))    /* Check if opened */
        return FR_NOT_OPENED;

    if(!btw)
    { /* Finalize request */
        if((fs->flag & FA__WIP) && DISKIO::disk_writep(0, 0)) goto fw_abort;
        fs->flag &= ~FA__WIP;
        return FR_OK;
    }
    else
    {                             /* Write data request */
        if(!(fs->flag & FA__WIP)) /* Round-down fptr to the sector boundary */
            fs->fptr &= 0xFFFFFE00;
    }
    remain = fs->fsize - fs->fptr;
    if(btw > remain) btw = (WORD)remain; /* Truncate btw by remaining bytes */

    while(btw)
    { /* Repeat until all data transferred */
        if(((WORD)fs->fptr % 512) == 0)
        {                                                  /* On the sector boundary? */
            cs = (BYTE)(fs->fptr / 512 & (fs->csize - 1)); /* Sector offset in the cluster */
            if(!cs)
            {                            /* On the cluster boundary? */
                clst = (fs->fptr == 0) ? /* On the top of the file? */
                           fs->org_clust
                                       : get_fat(fs->curr_clust);
                if(clst <= 1) goto fw_abort;
                fs->curr_clust = clst; /* Update current cluster */
            }
            sect = clust2sect(fs->curr_clust); /* Get current sector */
            if(!sect) goto fw_abort;
            fs->dsect = sect + cs;
            if(DISKIO::disk_writep(0, fs->dsect)) goto fw_abort; /* Initiate a sector write operation */
            fs->flag |= FA__WIP;
        }
        wcnt = 512 - ((WORD)fs->fptr % 512); /* Number of bytes to write to the sector */
        if(wcnt > btw) wcnt = btw;
        if(DISKIO::disk_writep(p, wcnt)) goto fw_abort; /* Send data to the sector */
        fs->fptr += wcnt;
        p += wcnt; /* Update pointers and counters */
        btw -= wcnt;
        *bw += wcnt;
        if(((WORD)fs->fptr % 512) == 0)
        {
            if(DISKIO::disk_writep(0, 0)) goto fw_abort; /* Finalize the currtent secter write operation */
            fs->flag &= ~FA__WIP;
        }
    }

    return FR_OK;

fw_abort:
    fs->flag = 0;
    return FR_DISK_ERR;
}

FRESULT PFF::lseek(DWORD ofs)
{
    CLUST clst;
    DWORD bcs, sect, ifptr;
    FATFS* fs = FatFs;

    if(!fs) return FR_NOT_ENABLED; /* Check file system */
    if(!(fs->flag & FA_OPENED))    /* Check if opened */
        return FR_NOT_OPENED;

    if(ofs > fs->fsize) ofs = fs->fsize; /* Clip offset with the file size */
    ifptr = fs->fptr;
    fs->fptr = 0;
    if(ofs > 0)
    {
        bcs = (DWORD)fs->csize * 512; /* Cluster size (byte) */
        if(ifptr > 0 && (ofs - 1) / bcs >= (ifptr - 1) / bcs)
        {                                        /* When seek to same or following cluster, */
            fs->fptr = (ifptr - 1) & ~(bcs - 1); /* start from the current cluster */
            ofs -= fs->fptr;
            clst = fs->curr_clust;
        }
        else
        {                         /* When seek to back cluster, */
            clst = fs->org_clust; /* start from the first cluster */
            fs->curr_clust = clst;
        }
        while(ofs > bcs)
        {                         /* Cluster following loop */
            clst = get_fat(clst); /* Follow cluster chain */
            if(clst <= 1 || clst >= fs->n_fatent) goto fe_abort;
            fs->curr_clust = clst;
            fs->fptr += bcs;
            ofs -= bcs;
        }
        fs->fptr += ofs;
        sect = clust2sect(clst); /* Current sector */
        if(!sect) goto fe_abort;
        fs->dsect = sect + (fs->fptr / 512 & (fs->csize - 1));
    }

    return FR_OK;

fe_abort:
    fs->flag = 0;
    return FR_DISK_ERR;
}

PFF::PFF() {}
