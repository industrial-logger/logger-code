/**
 * @file pwm.cpp
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Source file for PWM module
 * @version 0.1
 * @date 2021-12-12
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "pwm.hh"

PWM::PWM()
{
    TCCR0 = (1 << WGM00) | (1 << WGM01) | (1 << COM01) | (1 << CS00);
    TCCR2 = (1 << WGM00) | (1 << WGM01) | (1 << COM01) | (1 << CS00);
    _PWM_DDR_VOLTAGE |= (1 << _PWM_PIN_VOLTAGE);
    _PWM_DDR_CURRENT |= (1 << _PWM_PIN_CURRENT);
}

void PWM::set_voltage(float voltage)
{
    uint8_t duty = 0;
    duty = (uint8_t)((voltage / 10.0) * max_voltage_duty);

    _PWM_REGISTRY_VOLTAGE = duty;
}

void PWM::set_current(float current)
{
    uint8_t duty = 0;
    duty = (uint8_t)((current / 20.0) * max_current_duty);

    _PWM_REGISTRY_CURRENT = duty;
}
