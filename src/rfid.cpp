/**
 * @file rfid.cpp
 * @authors Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Source file for RFID scanner module
 * @version 0.1
 * @date 2021-10-21
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "rfid.hh"
#include "rfid_reg.hh"
#include "spi.hh"

void RFID::enable()
{
    _RFID_PORT &= ~(1 << _RFID_SS); // Make RFID_CS pin low
}

void RFID::disable()
{
    _RFID_PORT |= (1 << _RFID_SS); // Make RFID_CS pin high
}

void RFID::antenna_on()
{
    uint8_t value = MFRC522_read(_RFID_TXCONTROL_REG);
    if((value & 0x03) != 0x03) MFRC522_write(_RFID_TXCONTROL_REG, value | 0x03);
}

void RFID::set_bit_mask(uint8_t reg, uint8_t mask)
{
    uint8_t temp = MFRC522_read(reg);
    MFRC522_write(reg, temp | mask);
}

void RFID::clear_bit_mask(uint8_t reg, uint8_t mask)
{
    uint8_t temp = MFRC522_read(reg);
    MFRC522_write(reg, temp & (~mask));
}

uint8_t RFID::MFRC522_read(uint8_t address)
{
    uint8_t value;

    enable(); // Select slave
    // MSB == 1 is for reading. LSB is not used in address. Datasheet section 8.1.2.3.
    SPI::master_transmit(0x80 | ((address << 1) & 0x7E));
    value = SPI::master_transmit(0); // Read the value back. Send 0 to stop reading.
    disable();                       // Release slave again

    return value;
}

void RFID::MFRC522_read(uint8_t address, uint8_t count, uint8_t* values, uint8_t rx_align)
{
    if(count == 0) return;
    // MSB == 1 is for reading. LSB is not used in address. Datasheet section 8.1.2.3.
    uint8_t reg = 0x80 | ((address << 1) & 0x7E);
    uint8_t index = 0; // Index in values array.

    enable();                  // Select slave
    SPI::master_transmit(reg); // Tell MFRC522 which address we want to read
    while(index < count - 1)
    {
        if(index == 0 && rx_align) // Only update bit positions rxAlign..7 in values[0]
        {
            uint8_t mask = 0; // Create bit mask for bit positions rxAlign..7
            for(uint8_t i = rx_align; i < 7; ++i) mask |= (1 << i);
            uint8_t value =
                SPI::master_transmit(reg); // Read value and tell that we want to read the same address again.
            values[0] = (values[index] & ~mask) |
                        (value & mask); // Apply mask to both current value of values[0] and the new data in value.
        }
        else
        {
            values[index] =
                SPI::master_transmit(reg); // Read value and tell that we want to read the same address again.
        }
        index++;
    }
    values[index] = SPI::master_transmit(0); // Read the final byte. Send 0 to stop reading.
    disable();                               // Release slave again
}

void RFID::MFRC522_write(uint8_t address, uint8_t value)
{
    enable(); // Select slave
    // MSB == 0 is for writing. LSB is not used in address. Datasheet section 8.1.2.3.
    SPI::master_transmit((address << 1) & 0x7E);
    SPI::master_transmit(value); // Transfer value
    disable();                   // Release slave again
}

void RFID::MFRC522_write(uint8_t address, const uint8_t count, uint8_t* const values)
{
    enable(); // Select slave
    // MSB == 0 is for writing. LSB is not used in address. Datasheet section 8.1.2.3.
    SPI::master_transmit((address << 1) & 0x7E);
    for(uint8_t index = 0; index < count; index++) SPI::master_transmit(values[index]);
    disable(); // Release slave again
}

uint8_t RFID::MFRC522_communicate(uint8_t command, uint8_t waitIRq, uint8_t* send_data, uint8_t send_length,
                                  uint8_t* back_data /* = nullptr */, uint8_t* back_length /* = nullptr */,
                                  uint8_t* valid_bits /* = nullptr */, uint8_t rx_align /* = 0 */,
                                  bool check_CRC /* = false */)
{
    uint8_t tx_last_bits = valid_bits ? *valid_bits : 0;
    uint8_t bit_framing =
        (rx_align << 4) + tx_last_bits; // RxAlign = BitFramingReg[6..4]. TxLastBits = BitFramingReg[2..0]

    MFRC522_write(_RFID_COMMAND_REG, _RFID_PCD_IDLE);          // Stop any active command
    MFRC522_write(_RFID_COMIRQ_REG, 0x7F);                     // Clear all seven interupt request bits
    MFRC522_write(_RFID_FIFOLEVEL_REG, 0x80);                  // FlushBuffer = 1, FIFO initialization
    MFRC522_write(_RFID_FIFODATA_REG, send_length, send_data); // Write sendData to the FIFO
    MFRC522_write(_RFID_BITFRAMING_REG, bit_framing);          // Bit adjustments
    MFRC522_write(_RFID_COMMAND_REG, command);                 // Execute the command

    if(command == _RFID_PCD_TRANSCEIVE)
        set_bit_mask(_RFID_BITFRAMING_REG, 0x80); // StartSend=1, transmission of data starts

    // Wait for the command to complete.
    // In RFID() we set the TAuto flag in TModeReg. This means the timer automatically starts when the PCD stops
    // transmitting. Each iteration of the do-while-loop takes 17.86μs.
    uint16_t i;
    for(i = 2000; i > 0; i--)
    {
        uint8_t n = MFRC522_read(_RFID_COMIRQ_REG); // ComIrqReg[7..0] bits are: Set1 TxIRq RxIRq IdleIRq HiAlertIRq
                                                    // LoAlertIRq ErrIRq TimerIRq
        if(n & waitIRq) break;                      // One of the interrupts that signal success has been set.
        if(n & 0x01) return _RFID_STATUS_TIMEOUT;   // Timer interrupt - nothing received in 25ms
    }

    if(i == 0) return _RFID_STATUS_TIMEOUT; // 35.7ms and nothing happend. Communication with the MFRC522 might be down.

    // Stop now if any errors except collisions were detected.
    uint8_t error_register_value = MFRC522_read(_RFID_ERROR_REG); // ErrorReg[7..0] bits are: WrErr TempErr reserved
                                                                  // BufferOvfl CollErr CRCErr ParityErr ProtocolErr
    if(error_register_value & 0x13) return _RFID_STATUS_ERROR;    // BufferOvfl ParityErr ProtocolErr

    uint8_t _valid_bits = 0;

    // If the caller wants data back, get it from the MFRC522.
    if(back_data && back_length)
    {
        uint8_t n = MFRC522_read(_RFID_FIFOLEVEL_REG); // Number of bytes in the FIFO
        if(n > *back_length) return _RFID_STATUS_NO_ROOM;
        *back_length = n;                                         // Number of bytes returned
        MFRC522_read(_RFID_FIFODATA_REG, n, back_data, rx_align); // Get received data from FIFO
        _valid_bits =
            MFRC522_read(_RFID_CONTROL_REG) & 0x07; // RxLastBits[2:0] indicates the number of valid bits in the last
                                                    // received byte. If this value is 000b, the whole byte is valid.
        if(valid_bits) *valid_bits = _valid_bits;
    }

    // Tell about collisions
    if(error_register_value & 0x08) return _RFID_STATUS_COLLISION; // CollErr

    // Perform CRC_A validation if requested.
    if(back_data && back_length && check_CRC)
    {
        // In this case a MIFARE Classic NAK is not OK.
        if(*back_length == 1 && _valid_bits == 4) return _RFID_STATUS_MIFARE_NACK;

        // We need at least the CRC_A value and all 8 bits of the last byte must be received.
        if(*back_length < 2 || _valid_bits != 0) return _RFID_STATUS_CRC_WRONG;

        // Verify CRC_A - do our own calculation and store the control in controlBuffer.
        uint8_t control_buffer[2];
        uint8_t status = calculate_CRC(&back_data[0], *back_length - 2, &control_buffer[0]);
        if(status != _RFID_STATUS_OK) return status;

        if((back_data[*back_length - 2] != control_buffer[0]) || (back_data[*back_length - 1] != control_buffer[1]))
            return _RFID_STATUS_CRC_WRONG;
    }

    return _RFID_STATUS_OK;
}

uint8_t RFID::MFRC522_transceive_data(uint8_t* send_data, uint8_t send_length, uint8_t* back_data /* = nullptr */,
                                      uint8_t* back_length /* = nullptr */, uint8_t* valid_bits /* = nullptr */,
                                      uint8_t rx_align /* = 0 */, bool check_CRC /* = false */)
{
    uint8_t waitIRq = 0x30; // RxIRq and IdleIRq
    return MFRC522_communicate(_RFID_PCD_TRANSCEIVE, waitIRq, send_data, send_length, back_data, back_length,
                               valid_bits, rx_align, check_CRC);
}

uint8_t RFID::MFRC522_request_wakeup(uint8_t command, uint8_t* buffer_answer_request, uint8_t* buffer_size)
{
    if(buffer_answer_request == nullptr || *buffer_size < 2) // The ATQA response is 2 bytes long.
        return _RFID_STATUS_NO_ROOM;

    clear_bit_mask(_RFID_COLL_REG, 0x80); // ValuesAfterColl=1 => Bits received after collision are cleared.

    // For REQA and WUPA we need the short frame format - transmit only 7 bits of the last (and only) byte. TxLastBits =
    // BitFramingReg[2..0]
    uint8_t valid_bits = 7;
    uint8_t status = MFRC522_transceive_data(&command, 1, buffer_answer_request, buffer_size, &valid_bits);

    if(status != _RFID_STATUS_OK) return status;
    if(*buffer_size != 2 || valid_bits != 0) return _RFID_STATUS_ERROR; // ATQA must be exactly 16 bits.

    return _RFID_STATUS_OK;
}

uint8_t RFID::request(uint8_t* buffer_answer_request, uint8_t* buffer_size)
{
    return MFRC522_request_wakeup(_RFID_PICC_CMD_REQA, buffer_answer_request, buffer_size);
}

uint8_t RFID::wake_up(uint8_t* buffer_answer_request, uint8_t* buffer_size)
{
    return MFRC522_request_wakeup(_RFID_PICC_CMD_WUPA, buffer_answer_request, buffer_size);
}

uint8_t RFID::calculate_CRC(uint8_t* data, uint8_t length, uint8_t* result)
{
    MFRC522_write(_RFID_COMMAND_REG, _RFID_PCD_IDLE);     // Stop any active command.
    MFRC522_write(_RFID_DIVIRQ_REG, 0x04);                // CRCIrq = 0; Clear the CRCIRq interrupt request bit
    MFRC522_write(_RFID_FIFOLEVEL_REG, 0x80);             // FlushBuffer = 1, FIFO initialization
    MFRC522_write(_RFID_FIFODATA_REG, length, data);      // Write data to the FIFO
    MFRC522_write(_RFID_COMMAND_REG, _RFID_PCD_CALC_CRC); // Start the calculation

    // Wait for the CRC calculation to complete. Each iteration of the while-loop takes 17.73us.
    for(uint16_t i = 5000; i > 0; --i)
    {
        // DivIrqReg[7..0] bits are: Set2 reserved reserved MfinActIRq reserved CRCIRq reserved reserved
        uint8_t n = MFRC522_read(_RFID_DIVIRQ_REG);
        if(n & 0x04)
        {
            // Stop calculating CRC for new content in the FIFO
            MFRC522_write(_RFID_COMMAND_REG, _RFID_PCD_IDLE);
            // Transfer the result from the registers to the result buffer
            result[0] = MFRC522_read(_RFID_CRCRESULT_REG_L);
            result[1] = MFRC522_read(_RFID_CRCRESULT_REG_M);
            return _RFID_STATUS_OK;
        }
    }
    // 89ms passed and nothing happend. Communication with the MFRC522 might be down.
    return _RFID_STATUS_TIMEOUT;
}

void RFID::reset()
{
    MFRC522_write(_RFID_COMMAND_REG, _RFID_PCD_SOFT_RESET); // Issue the SoftReset command.
    // The datasheet does not mention how long the SoftRest command takes to complete. But the MFRC522 might have been
    // in soft power-down mode (triggered by bit 4 of CommandReg) Section 8.8.2 in the datasheet says the oscillator
    // start-up time is the start up time of the crystal + 37,74μs. Let us be generous: 50ms.
    uint8_t count = 0;
    do
    {
        // Wait for the PowerDown bit in CommandReg to be cleared (max 3x50ms)
        _delay_ms(50);
    } while((MFRC522_read(_RFID_COMMAND_REG) & (1 << 4)) && (++count) < 3);
}

uint8_t RFID::select_picc(UID* uid, uint8_t valid_bits /* = 0 */)
{
    bool uid_complete, select_done, use_cascade_tag;
    uint8_t cascade_level = 1, result, count, check_bit, index;
    uint8_t uid_index;               // The first index in uid->uidByte[] that is used in the current Cascade Level.
    int8_t current_level_known_bits; // The number of known UID bits in the current Cascade Level.
    uint8_t buffer[9];               // The SELECT/ANTICOLLISION commands uses a 7 byte standard frame + 2 bytes CRC_A
    uint8_t buffer_used;  // The number of bytes used in the buffer, ie the number of bytes to transfer to the FIFO.
    uint8_t rx_align;     // Used in BitFramingReg. Defines the bit position for the first bit received.
    uint8_t tx_last_bits; // Used in BitFramingReg. The number of valid bits in the last transmitted byte.
    uint8_t* response_buffer;
    uint8_t response_length;

    // Sanity checks
    if(valid_bits > 80) return _RFID_STATUS_INVALID;

    // Prepare MFRC522
    clear_bit_mask(_RFID_COLL_REG, 0x80); // ValuesAfterColl=1 => Bits received after collision are cleared.

    // Repeat Cascade Level loop until we have a complete UID.
    uid_complete = false;
    while(!uid_complete)
    {
        // Set the Cascade Level in the SEL byte, find out if we need to use the Cascade Tag in byte 2.
        switch(cascade_level)
        {
            case 1:
                buffer[0] = _RFID_PICC_CMD_SEL_CL1;
                uid_index = 0;
                use_cascade_tag = valid_bits && uid->size > 4; // When we know that the UID has more than 4 bytes
                break;

            case 2:
                buffer[0] = _RFID_PICC_CMD_SEL_CL2;
                uid_index = 3;
                use_cascade_tag = valid_bits && uid->size > 7; // When we know that the UID has more than 7 bytes
                break;

            case 3:
                buffer[0] = _RFID_PICC_CMD_SEL_CL3;
                uid_index = 6;
                use_cascade_tag = false; // Never used in CL3.
                break;

            default:
                return _RFID_STATUS_INTERNAL_ERROR;
                break;
        }

        // How many UID bits are known in this Cascade Level?
        current_level_known_bits = valid_bits - (8 * uid_index);
        if(current_level_known_bits < 0) current_level_known_bits = 0;
        // Copy the known bits from uid->uidByte[] to buffer[]
        index = 2; // destination index in buffer[]
        if(use_cascade_tag)
        {
            buffer[index++] = _RFID_PICC_CMD_CT;
        }
        uint8_t bytes_to_copy = current_level_known_bits / 8 +
                                (current_level_known_bits % 8
                                     ? 1
                                     : 0); // The number of bytes needed to represent the known bits for this level.
        if(bytes_to_copy)
        {
            uint8_t max_bytes =
                use_cascade_tag ? 3 : 4; // Max 4 bytes in each Cascade Level. Only 3 left if we use the Cascade Tag
            if(bytes_to_copy > max_bytes) bytes_to_copy = max_bytes;
            for(count = 0; count < bytes_to_copy; count++) buffer[index++] = uid->uid_byte[uid_index + count];
        }
        // Now that the data has been copied we need to include the 8 bits in CT in currentLevelKnownBits
        if(use_cascade_tag) current_level_known_bits += 8;

        // Repeat anti collision loop until we can transmit all UID bits + BCC and receive a SAK - max 32 iterations.
        select_done = false;
        while(!select_done)
        {
            // Find out how many bits and bytes to send and receive.
            if(current_level_known_bits >= 32)
            {                     // All UID bits in this Cascade Level are known. This is a SELECT.
                buffer[1] = 0x70; // NVB - Number of Valid Bits: Seven whole bytes
                // Calculate BCC - Block Check Character
                buffer[6] = buffer[2] ^ buffer[3] ^ buffer[4] ^ buffer[5];
                // Calculate CRC_A
                result = calculate_CRC(buffer, 7, &buffer[7]);
                if(result != _RFID_STATUS_OK) return result;
                tx_last_bits = 0; // 0 => All 8 bits are valid.
                buffer_used = 9;
                // Store response in the last 3 bytes of buffer (BCC and CRC_A - not needed after tx)
                response_buffer = &buffer[6];
                response_length = 3;
            }
            else
            { // This is an ANTICOLLISION.
                tx_last_bits = current_level_known_bits % 8;
                count = current_level_known_bits / 8;    // Number of whole bytes in the UID part.
                index = 2 + count;                       // Number of whole bytes: SEL + NVB + UIDs
                buffer[1] = (index << 4) + tx_last_bits; // NVB - Number of Valid Bits
                buffer_used = index + (tx_last_bits ? 1 : 0);
                // Store response in the unused part of buffer
                response_buffer = &buffer[index];
                response_length = sizeof(buffer) - index;
            }

            // Set bit adjustments
            rx_align =
                tx_last_bits; // Having a separate variable is overkill. But it makes the next line easier to read.
            MFRC522_write(_RFID_BITFRAMING_REG,
                          (rx_align << 4) +
                              tx_last_bits); // RxAlign = BitFramingReg[6..4]. TxLastBits = BitFramingReg[2..0]

            // Transmit the buffer and receive the response.
            result = MFRC522_transceive_data(buffer, buffer_used, response_buffer, &response_length, &tx_last_bits,
                                             rx_align);
            if(result == _RFID_STATUS_COLLISION)
            { // More than one PICC in the field => collision.
                uint8_t value_collision_register = MFRC522_read(
                    _RFID_COLL_REG); // CollReg[7..0] bits are: ValuesAfterColl reserved CollPosNotValid CollPos[4:0]
                if(value_collision_register & 0x20)
                {                                  // CollPosNotValid
                    return _RFID_STATUS_COLLISION; // Without a valid collision position we cannot continue
                }
                uint8_t collision_position = value_collision_register & 0x1F; // Values 0-31, 0 means bit 32.
                if(collision_position == 0)
                {
                    collision_position = 32;
                }
                if(collision_position <= current_level_known_bits)
                { // No progress - should not happen
                    return _RFID_STATUS_INTERNAL_ERROR;
                }
                // Choose the PICC with the bit set.
                current_level_known_bits = collision_position;
                count = current_level_known_bits % 8; // The bit to modify
                check_bit = (current_level_known_bits - 1) % 8;
                index = 1 + (current_level_known_bits / 8) + (count ? 1 : 0); // First byte is index 0.
                buffer[index] |= (1 << check_bit);
            }
            else if(result != _RFID_STATUS_OK)
            {
                return result;
            }
            else
            { // StatusCode::STATUS_OK
                if(current_level_known_bits >= 32)
                {                       // This was a SELECT.
                    select_done = true; // No more anticollision
                                        // We continue below outside the while.
                }
                else
                { // This was an ANTICOLLISION.
                    // We now have all 32 bits of the UID in this Cascade Level
                    current_level_known_bits = 32;
                    // Run loop again to do the SELECT.
                }
            }
        } // End of while (!selectDone)

        // We do not check the CBB - it was constructed by us above.

        // Copy the found UID bytes from buffer[] to uid->uidByte[]
        index = (buffer[2] == _RFID_PICC_CMD_CT) ? 3 : 2; // source index in buffer[]
        bytes_to_copy = (buffer[2] == _RFID_PICC_CMD_CT) ? 3 : 4;
        for(count = 0; count < bytes_to_copy; count++)
        {
            uid->uid_byte[uid_index + count] = buffer[index++];
        }

        // Check response SAK (Select Acknowledge)
        if(response_length != 3 || tx_last_bits != 0)
        { // SAK must be exactly 24 bits (1 byte + CRC_A).
            return _RFID_STATUS_ERROR;
        }
        // Verify CRC_A - do our own calculation and store the control in buffer[2..3] - those bytes are not needed
        // anymore.
        result = calculate_CRC(response_buffer, 1, &buffer[2]);
        if(result != _RFID_STATUS_OK) return result;
        if((buffer[2] != response_buffer[1]) || (buffer[3] != response_buffer[2])) return _RFID_STATUS_CRC_WRONG;
        if(response_buffer[0] & 0x04)
        { // Cascade bit set - UID not complete yes
            cascade_level++;
        }
        else
        {
            uid_complete = true;
            uid->sak = response_buffer[0];
        }
    } // End of while (!uidComplete)

    // Set correct uid->size
    uid->size = 3 * cascade_level + 1;

    return _RFID_STATUS_OK;
}

uint8_t RFID::halt()
{
    uint8_t result;
    uint8_t buffer[4];

    // Build command buffer
    buffer[0] = _RFID_PICC_CMD_HLTA;
    buffer[1] = 0;
    // Calculate CRC_A
    result = calculate_CRC(buffer, 2, &buffer[2]);
    if(result != _RFID_STATUS_OK) return result;

    // Send the command.
    // The standard says:
    // If the PICC responds with any modulation during a period of 1 ms after the end of the frame containing
    // the HLTA command, this response shall be interpreted as 'not acknowledge'.
    // We interpret that this way: Only STATUS_TIMEOUT is an success.
    result = MFRC522_transceive_data(buffer, sizeof(buffer));
    if(result == _RFID_STATUS_TIMEOUT) return _RFID_STATUS_OK;
    if(result == _RFID_STATUS_OK) return _RFID_STATUS_ERROR; // That is ironically NOT ok in this case ;-)

    return result;
}

RFID::RFID()
{
// Soft reset has no disadvantage so there is no reason to use hard reset.
#ifdef _RFID_RST
    _RFID_DDR |= (1 << _RFID_RST);
    _RFID_PORT |= (1 << _RFID_RST);
    _delay_ms(50);
#endif

    // Perform a soft reset
    reset();
    _delay_ms(50);

    // Reset baud rates
    MFRC522_write(_RFID_TXMODE_REG, 0x00);
    MFRC522_write(_RFID_TXMODE_REG, 0x00);

    // Reset ModWidthReg
    MFRC522_write(_RFID_MODWIDTH_REG, 0x26);

    // When communicating with a PICC we need a timeout if something goes wrong. f_timer = 13.56 MHz / (2*TPreScaler+1)
    // where TPreScaler = [TPrescaler_Hi:TPrescaler_Lo]. TPrescaler_Hi are the four low bits in TModeReg. TPrescaler_Lo
    // is TPrescalerReg.

    // TAuto=1; timer starts automatically at the end of the transmission in all communication modes at all speeds
    MFRC522_write(_RFID_TMODE_REG, 0x80);

    // TPreScaler = TModeReg[3..0]:TPrescalerReg, ie 0x0A9 = 169 => f_timer=40kHz, ie a timer period of 25μs.
    MFRC522_write(_RFID_TPRESCALER_REG, 0xA9);

    // Reload timer with 0x3E8 = 1000, ie 25ms before timeout.
    MFRC522_write(_RFID_TRELOAD_REG_H, 0x03);
    MFRC522_write(_RFID_TRELOAD_REG_L, 0xE8);

    // Default 0x00. Force a 100 % ASK modulation independent of the ModGsPReg register setting
    MFRC522_write(_RFID_TXASK_REG, 0x40);

    // Default 0x3F. Set the preset value for the CRC coprocessor for the CalcCRC command to 0x6363 (ISO 14443-3
    // part 6.2.4)
    MFRC522_write(_RFID_MODE_REG, 0x3D);

    // Enable the antenna driver pins TX1 and TX2 (they were disabled by the reset)
    antenna_on();

    _delay_ms(4);
}

bool RFID::is_card()
{
    uint8_t buffer_ATQA[2];
    uint8_t buffer_size = sizeof(buffer_ATQA);

    MFRC522_write(_RFID_TXMODE_REG, 0x00); // Reset baud rates
    MFRC522_write(_RFID_RXMODE_REG, 0x00);
    MFRC522_write(_RFID_MODWIDTH_REG, 0x26); // Reset ModWidthReg

    uint8_t result = request(buffer_ATQA, &buffer_size);
    return (result == _RFID_STATUS_OK || result == _RFID_STATUS_COLLISION);
}

bool RFID::read_card_serial()
{
    return (select_picc(&uid) == _RFID_STATUS_OK);
}

uint8_t RFID::get_uid_size()
{
    return uid.size;
}

uint8_t* RFID::get_card_uid()
{
    halt();
    return uid.uid_byte;
}

uint8_t RFID::version()
{
    return MFRC522_read(_RFID_VERSION_REG);
}
