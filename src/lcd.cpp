/**
 * @file lcd.cpp
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Source file for LCD module
 * @version 0.1
 * @date 2021-10-21
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "lcd.hh"

void LCD::command(uint8_t cmd)
{
    _LCD_PORT = (_LCD_PORT & 0x0F) | (cmd & 0xF0);
    _LCD_PORT &= ~(1 << _LCD_RS);
    _LCD_PORT &= ~(1 << _LCD_RW);
    _LCD_PORT |= (1 << _LCD_EN);
    _delay_us(1);
    _LCD_PORT &= ~(1 << _LCD_EN);
    _delay_us(200);

    _LCD_PORT = (_LCD_PORT & 0x0F) | (cmd << 4);
    _LCD_PORT |= (1 << _LCD_EN);
    _delay_us(1);
    _LCD_PORT &= ~(1 << _LCD_EN);
    _delay_ms(4.1);
}

void LCD::write(uint8_t sign)
{
    _LCD_PORT = (_LCD_PORT & 0x0F) | (sign & 0xF0);
    _LCD_PORT |= (1 << _LCD_RS);
    _LCD_PORT &= ~(1 << _LCD_RW);
    _LCD_PORT |= (1 << _LCD_EN);
    _delay_us(1);
    _LCD_PORT &= ~(1 << _LCD_EN);
    _delay_us(200);

    _LCD_PORT = (_LCD_PORT & 0x0F) | (sign << 4);
    _LCD_PORT |= (1 << _LCD_EN);
    _delay_us(1);
    _LCD_PORT &= ~(1 << _LCD_EN);
    _delay_ms(4.1);
}

LCD::LCD()
{
    _LCD_DDR |= (1 << _LCD_RS) | (1 << _LCD_RW) | (1 << _LCD_EN);
    _LCD_DDR |= (1 << _LCD_D4) | (1 << _LCD_D5) | (1 << _LCD_D6) | (1 << _LCD_D7);
    _delay_ms(15);

    _LCD_PORT &= ~(1 << _LCD_EN) & ~(1 << _LCD_RS);
    _delay_ms(4.1);

    command(_LCD_4BIT_MODE);
    command(_LCD_2LINE_4BIT);
    command(_LCD_CURSOR_OFF);
    command(_LCD_INCREMENT);
    clear();
}

void LCD::clear()
{
    command(_LCD_CLEAR_DISPLAY);
    command(_LCD_FIRST_LINE);
}

void LCD::display(uint8_t string[])
{
    for(int i = 0; string[i] != 0; i++)
    {
        write(string[i]);
    }
}

void LCD::set_position(uint8_t y, uint8_t x)
{
    uint8_t pos = 0;

    if(y == 0)
        pos = _LCD_FIRST_LINE;
    else if(y == 1)
        pos = _LCD_SECOND_LINE;

    if(x < _LCD_COL_COUNT) pos += x;

    command(pos);
}

void LCD::shift_left()
{
    command(_LCD_SHIFT_LEFT);
}

void LCD::shift_right()
{
    command(_LCD_SHIFT_RIGHT);
}

void LCD::roll()
{
    _delay_ms(500);
    int shift = 15;

    for(int i = 0; i < shift; i++)
    {
        shift_left();
        _delay_ms(500);
    }

    for(int i = 0; i < shift; i++)
    {
        shift_right();
        _delay_ms(500);
    }
}

void LCD::display_on_pos(uint8_t y, uint8_t x, uint8_t string[])
{
    set_position(y, x);
    display(string);
}

void LCD::display_on_pos_hex(uint8_t y, uint8_t x, uint8_t d)
{
    set_position(y, x);
    uint8_t byte = '0';
    if(((d >> 4) & 0x0F) <= 9)
        byte = '0' + ((d >> 4) & 0x0F);
    else
        byte = 'A' + ((d >> 4) & 0x0F) - 0x0A;

    write(byte);
    _delay_us(200);

    if((d & 0x0F) <= 9)
        byte = '0' + (d & 0x0F);
    else
        byte = 'A' + (d & 0x0F) - 0x0A;

    write(byte);
}
