/**
 * @file spi.cpp
 * @authors Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Source file for SPI module
 * @version 0.1
 * @date 2021-10-21
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "spi.hh"

SPI::SPI()
{
    _SPI_DDR |= (1 << _SPI_SS) | (1 << _SPI_CS_1) | (1 << _SPI_CS_2) | (1 << _SPI_MOSI) | (1 << _SPI_SCK);
    _SPI_PORT |= (1 << _SPI_SS) | (1 << _SPI_CS_1) | (1 << _SPI_CS_2);

    SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR1); // Fosc/64
}

uint8_t SPI::master_transmit(uint8_t data)
{

    SPDR = data; // load data into register
    while(!(SPSR & (1 << SPIF)))
        ; // wait for transmission to complete

    return SPDR;
}

uint8_t SPI::master_read() // receiving
{
    SPDR = 0xFF; // transmit dummy byte
    while(!(SPSR & (1 << SPIF)))
        ; // wait for transmission to complete

    return SPDR;
}
