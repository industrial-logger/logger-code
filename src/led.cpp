/**
 * @file led.cpp
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Source file for LED module
 * @version 0.1
 * @date 2021-10-21
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "led.hh"

LED::LED()
{
    _LED_DDR |= (1 << _LED_RED) | (1 << _LED_YELLOW) | (1 << _LED_GREEN);
    _LED_PORT &= ~(1 << _LED_RED) & ~(1 << _LED_YELLOW) & ~(1 << _LED_GREEN);
}

void LED::turn_on(int led_pin)
{
    _LED_PORT |= (1 << led_pin);
}

void LED::turn_off(int led_pin)
{
    _LED_PORT &= ~(1 << led_pin);
}