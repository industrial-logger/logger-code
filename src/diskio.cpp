/**
 * @file diskio.hh
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Source file for SD Card module. Low level disk control module for AVR
 * @version 0.1
 * @date 2022-01-10
 *
 * @copyright Copyright (C) 2010, ChaN
 *
 */

#include "diskio.hh"
#include "pff.hh"
#include "spi.hh"
#include <avr/io.h>
#include <util/delay.h>

BYTE DISKIO::CardType;

void DISKIO::enable()
{
    _SD_PORT &= ~(1 << _SD_CS);
}

void DISKIO::disable()
{
    _SD_PORT |= (1 << _SD_CS);
}

BYTE DISKIO::send_cmd(BYTE cmd, DWORD arg)
{
    BYTE n, res;

    if(cmd & 0x80)
    { /* ACMD<n> is the command sequense of CMD55-CMD<n> */
        cmd &= 0x7F;
        res = send_cmd(CMD55, 0);
        if(res > 1) return res;
    }

    /* Select the card */
    enable();

    SPI::master_read();
    enable();
    SPI::master_read();

    /* Send a command packet */
    SPI::master_transmit(cmd); /* Start + Command index */
    SPI::master_transmit((uint8_t)(arg >> 24));
    SPI::master_transmit((uint8_t)(arg >> 16));
    SPI::master_transmit((uint8_t)(arg >> 8));
    SPI::master_transmit((uint8_t)(arg));
    n = 0x01;                 /* Dummy CRC + Stop */
    if(cmd == CMD0) n = 0x95; /* Valid CRC for CMD0(0) */
    if(cmd == CMD8) n = 0x87; /* Valid CRC for CMD8(0x1AA) */
    SPI::master_transmit(n);

    /* Receive a command response */
    n = 10; /* Wait for a valid response in timeout of 10 attempts */
    do
    {
        res = SPI::master_read();
        ;
    } while((res & 0x80) && --n);

    return res; /* Return with the response value */
}

DISKIO::DISKIO()
{
    disk_initialize();
}

DSTATUS DISKIO::disk_initialize()
{
    BYTE n, cmd, ty, ocr[4];
    UINT tmr;

    if(CardType && MMC_SEL) disk_writep(0, 0); /* Finalize write process if it is in progress */

    /* Initialize ports to control MMC */
    enable();

    _delay_ms(500);
    for(n = 10; n; n--) SPI::master_read(); /* 80 dummy clocks with CS=H */

    ty = 0;
    if(send_cmd(CMD0, 0) == 1)
    { /* Enter Idle state */
        if(send_cmd(CMD8, 0x1AA) == 1)
        {                                                       /* SDv2 */
            for(n = 0; n < 4; n++) ocr[n] = SPI::master_read(); /* Get trailing return value of R7 resp */
            if(ocr[2] == 0x01 && ocr[3] == 0xAA)
            { /* The card can work at vdd range of 2.7-3.6V */
                for(tmr = 10000; tmr && send_cmd(ACMD41, 1UL << 30); tmr--)
                    _delay_ms(100); /* Wait for leaving idle state (ACMD41 with HCS bit) */
                if(tmr && send_cmd(CMD58, 0) == 0)
                { /* Check CCS bit in the OCR */
                    for(n = 0; n < 4; n++) ocr[n] = SPI::master_read();
                    ty = (ocr[0] & 0x40) ? CT_SD2 | CT_BLOCK : CT_SD2; /* SDv2 (HC or SC) */
                }
            }
        }
        else
        { /* SDv1 or MMCv3 */
            if(send_cmd(ACMD41, 0) <= 1)
            {
                ty = CT_SD1;
                cmd = ACMD41; /* SDv1 */
            }
            else
            {
                ty = CT_MMC;
                cmd = CMD1; /* MMCv3 */
            }
            for(tmr = 10000; tmr && send_cmd(cmd, 0); tmr--) _delay_ms(100); /* Wait for leaving idle state */
            if(!tmr || send_cmd(CMD16, 512) != 0)                            /* Set R/W block length to 512 */
                ty = 0;
        }
    }
    CardType = ty;
    enable();

    SPI::master_read();

    return ty ? 0 : STA_NOINIT;
}

DRESULT DISKIO::disk_readp(BYTE* buff, DWORD lba, WORD ofs, WORD cnt)
{
    DRESULT res;
    BYTE rc;
    WORD bc;

    if(!(CardType & CT_BLOCK)) lba *= 512; /* Convert to byte address if needed */

    res = RES_ERROR;
    if(send_cmd(CMD17, lba) == 0)
    { /* READ_SINGLE_BLOCK */

        bc = 40000;
        do
        { /* Wait for data packet */
            rc = SPI::master_read();
        } while(rc == 0xFF && --bc);

        if(rc == 0xFE)
        { /* A data packet arrived */
            bc = 514 - ofs - cnt;

            /* Skip leading bytes */
            if(ofs)
            {
                do SPI::master_read();
                while(--ofs);
            }

            /* Receive a part of the sector */
            if(buff)
            { /* Store data to the memory */
                do
                {
                    *buff++ = SPI::master_read();
                } while(--cnt);
            }
            /* Skip trailing bytes and CRC */
            do SPI::master_read();
            while(--bc);

            res = RES_OK;
        }
    }

    disable();
    SPI::master_read();

    return res;
}

DRESULT DISKIO::disk_writep(const BYTE* buff, DWORD sa)
{
    DRESULT res;
    WORD bc;
    static WORD wc;

    res = RES_ERROR;

    if(buff)
    { /* Send data bytes */
        bc = (WORD)sa;
        while(bc && wc)
        { /* Send data bytes to the card */
            SPI::master_transmit(*buff++);
            wc--;
            bc--;
        }
        res = RES_OK;
    }
    else
    {
        if(sa)
        {                                         /* Initiate sector write process */
            if(!(CardType & CT_BLOCK)) sa *= 512; /* Convert to byte address if needed */
            if(send_cmd(CMD24, sa) == 0)
            { /* WRITE_SINGLE_BLOCK */
                SPI::master_transmit(0xFF);
                SPI::master_transmit(0xFE); /* Data block header */
                wc = 512;                   /* Set byte counter */
                res = RES_OK;
            }
        }
        else
        { /* Finalize sector write process */
            bc = wc + 2;
            while(bc--) SPI::master_transmit(0); /* Fill left bytes and CRC with zeros */
            if((SPI::master_read() & 0x1F) == 0x05)
            { /* Receive data resp and wait for end of write process in timeout of 500ms */
                for(bc = 5000; SPI::master_read() != 0xFF && bc; bc--) _delay_us(100); /* Wait ready */
                if(bc) res = RES_OK;
            }
            disable();
            SPI::master_read();
        }
    }

    return res;
}
