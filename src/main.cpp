#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <avr/io.h>
#include <util/delay.h>

#include "adc.hh"
#include "interruptions.hh"
#include "lcd.hh"
#include "led.hh"
#include "pff.hh"
#include "pwm.hh"
#include "rfid.hh"
#include "spi.hh"

int main(void)
{
    INTERRUPT interrupt;
    LED led;
    LCD lcd;
    SPI spi;
    RFID rfid;

    led.turn_on(_LED_RED);
    led.turn_on(_LED_YELLOW);
    led.turn_on(_LED_GREEN);
    _delay_ms(1000);
    led.turn_off(_LED_RED);
    led.turn_off(_LED_GREEN);
    _delay_ms(200);

    uint8_t version = rfid.version();
    if(version != 0x92 && version != 0x91 && version != 0x90)
    {
        led.turn_on(_LED_RED);
        lcd.display_on_pos(0, 0, (uint8_t*)"RFID - Error");
        lcd.display_on_pos(1, 0, (uint8_t*)"No reader found");
        _delay_ms(1500);
        lcd.clear();
        led.turn_off(_LED_RED);
        return -1;
    }

    lcd.display_on_pos(0, 3, (uint8_t*)"LOG IN TO");
    lcd.display_on_pos(1, 1, (uint8_t*)"UNLOCK DEVICE");
    while(!rfid.is_card())
        ;
    lcd.clear();

    lcd.display_on_pos(0, 5, (uint8_t*)"LOGIN");
    lcd.display_on_pos(1, 2, (uint8_t*)"SUCCESSFULLY");
    led.turn_off(_LED_YELLOW);
    led.turn_on(_LED_GREEN);
    _delay_ms(1000);
    led.turn_off(_LED_GREEN);
    lcd.clear();

    lcd.display_on_pos(0, 3, (uint8_t*)"STARTING");
    lcd.display_on_pos(1, 4, (uint8_t*)"DEVICE");
    _delay_ms(500);

    DISKIO sd;
    FATFS fs;
    PFF pff;
    ADC_ adc;
    PWM pwm;
    WORD bw;
    FRESULT res;

    pff.mount(&fs);
    res = pff.open("data.txt");
    if(res != FR_OK)
    {
        lcd.clear();
        led.turn_on(_LED_RED);
        lcd.display_on_pos(0, 0, (uint8_t*)"SD - Error");
        lcd.display_on_pos(1, 0, (uint8_t*)"Something wrong");
        _delay_ms(1500);
        lcd.clear();
        led.turn_off(_LED_RED);
        return -1;
    }

    lcd.clear();
    lcd.display_on_pos(0, 3, (uint8_t*)"DATALOGGER");
    lcd.display_on_pos(1, 3, (uint8_t*)"ACTIVATED");
    _delay_ms(1500);
    lcd.clear();

    uint16_t character_offset = 0;
    char buffer[80], temp_buff[6];
    double adc_measurement = 0.0, current_measurement = 0.0, voltage_measurement = 0.0;

    pff.lseek(fs.fptr);

    while(!interrupt.is_nok())
    {
        lcd.display_on_pos(0, 1, (uint8_t*)"Running cycle");
        lcd.display_on_pos(1, 0, (uint8_t*)"ADC");
        adc_measurement = adc.read();
        dtostrf(adc_measurement, 3, 3, temp_buff);
        character_offset += sprintf(buffer + character_offset, "ADC: %s\n", temp_buff);
        _delay_ms(700);
        lcd.clear();

        lcd.display_on_pos(0, 1, (uint8_t*)"Running cycle");
        lcd.display_on_pos(1, 0, (uint8_t*)"PWM -> 4..20mA");
        current_measurement = adc_measurement * 1.6 + 4.0;
        if(current_measurement > 20) current_measurement = 20;
        pwm.set_current(current_measurement);
        dtostrf(current_measurement, 3, 3, temp_buff);
        character_offset += sprintf(buffer + character_offset, "PWM -> 4..20mA: %s\n", temp_buff);
        _delay_ms(700);
        lcd.clear();

        lcd.display_on_pos(0, 1, (uint8_t*)"Running cycle");
        lcd.display_on_pos(1, 0, (uint8_t*)"PWM -> 0-10V");
        voltage_measurement = adc_measurement / 2.0;
        pwm.set_voltage(voltage_measurement);
        dtostrf(voltage_measurement, 3, 3, temp_buff);
        character_offset += sprintf(buffer + character_offset, "PWM -> 0-10V: %s\n", temp_buff);
        _delay_ms(700);
        lcd.clear();

        if(interrupt.is_ok())
        {
            interrupt.clear_ok();
            lcd.display_on_pos(0, 1, (uint8_t*)"Running cycle");
            lcd.display_on_pos(1, 0, (uint8_t*)"INTERRUPTS");
            character_offset += sprintf(buffer + character_offset, "INTERRUPTS: Part is OK\n");
            _delay_ms(700);
            lcd.clear();
        }

        pff.write(buffer, strlen(buffer), &bw);
        led.turn_on(_LED_YELLOW);
        _delay_ms(500);
        led.turn_off(_LED_YELLOW);
        character_offset = 0;

        led.turn_on(_LED_GREEN);
        _delay_ms(1000);
        led.turn_off(_LED_GREEN);
    }

    lcd.clear();
    interrupt.clear_nok();
    led.turn_on(_LED_RED);
    pff.write(0, 0, &bw);
    lcd.display_on_pos(0, 0, (uint8_t*)"END OF WORK");
}
