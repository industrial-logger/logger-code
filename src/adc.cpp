/**
 * @file adc.cpp
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Source file for ADC module
 * @version 0.1
 * @date 2022-01-28
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "adc.hh"

ADC_::ADC_()
{
    DDRA |= (0 << PA0);
    ADMUX |= (1 << REFS0);
    ADCSRA |= (1 << ADEN) | (1 << ADPS0) | (1 << ADPS1);
}

double ADC_::read()
{
    ADCSRA |= (1 << ADSC); /* Start conversion */
    while((ADCSRA & (1 << ADIF)) == 0)
        ; /* Monitor end of conversion interrupt */

    _delay_us(10);
    ADCSRA |= 0x10; /* Clear interrupt flag by writing 1 to it */

    measurement = ((ADC * 3.3) / 1024.0) * 2.36;

    return measurement;
}
