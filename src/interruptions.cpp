/**
 * @file interruptions.cpp
 * @author Jakub Kozłowicz, Aleksandra Rozmus
 * @brief Source file for digital measurement module
 * @version 0.1
 * @date 2022-01-25
 *
 * @copyright Copyright (c) 2021
 *
 */

#include "interruptions.hh"

uint8_t INTERRUPT::ok = 0;
uint8_t INTERRUPT::nok = 0;

INTERRUPT::INTERRUPT()
{
    sei();
    DDRD &= ~(1 << _SW_OK);
    PORTD |= (1 << _SW_OK);
    DDRD &= ~(1 << _SW_NOK);
    PORTD |= (1 << _SW_NOK);

    MCUCR |= (1 << ISC01) | (1 << ISC11);
    GICR |= (1 << INT0) | (1 << INT1);
}

ISR(INT0_vect)
{
    INTERRUPT::set_ok();
}

ISR(INT1_vect)
{
    INTERRUPT::set_nok();
}
